# Contributing

## Dependencies & peerDependencies

When a component requires a dependency, it can be added in package.json in the "dependency" array.  
We assume our the users of design-system don't use that dependency also.

However if a consumer of design-system does use it, there may be a version conflict. In that case we'll move it to the "peerDependencies" array with the applicable version range.

## Semantic Release
This project uses [semantic-release](https://semantic-release.gitbook.io/semantic-release/). When merging a MR to main, this will automatically generate our [CHANGELOG](./CHANGELOG.md) based on the commit messages and a version tag will be added.

## Conventions for commit messages
We follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification. This is enforced with a linter in the build pipeline. This convention requires you to pas a type and an optional scope as the commit message. The scope is based on the applications in the repository. If you are not sure which scope to use please leave the scope empty.

The type must be one of the following:

- **build**: Changes that affect the build system or external dependencies
- **ci**: Changes to our CI configuration files and scripts
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **revert**: Changes that revert other changes
- **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- **test**: Adding missing tests or correcting existing tests

The scope can be left empty.
