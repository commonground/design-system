// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

const { defineConfig } = require('cypress')

module.exports = defineConfig({
  e2e: {
    baseUrl: 'http://localhost:9001/',
    video: false,
    fixturesFolder: false,
  },
})
