// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { axeRunOptions, terminalLog } from './axe'

const visitStory = (...path) => {
  cy.visit('/iframe.html', {
    qs: {
      id: `components-${path.join('--')}`,
      viewMode: 'story',
    },
  })

  cy.get('body')
    .should('have.class', 'sb-show-main')
    .and('not.have.class', 'sb-show-errordisplay')
}

const checkStory = (...path) => {
  visitStory(...path)
  cy.injectAxe()
  cy.checkA11y('#storybook-root', axeRunOptions, terminalLog)
}

const currentComponent = () => {
  return Cypress.currentTest.titlePath[0].toLowerCase()
}

export { checkStory, currentComponent }
