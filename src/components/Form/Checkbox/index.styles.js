// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Label } from '../index'

export const StyledLabel = styled(Label)`
  color: ${(p) => p.colorText};
  display: inline-flex;
  align-items: center;
  line-height: 1rem;
  margin-right: ${(p) => p.theme.tokens.spacing08};
  cursor: ${(p) => (p.disabled ? 'auto' : 'pointer')};

  input {
    margin-right: ${(p) => p.theme.tokens.spacing04};
  }
`
