// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { bool, string } from 'prop-types'
import { Field, useField } from 'formik'
import { StyledLabel } from './index.styles'
import Input from './Input'

const Checkbox = ({ children, ...props }) => {
  const [field] = useField({ ...props, type: 'checkbox' })
  const { disabled } = props

  return (
    <StyledLabel disabled={disabled ? true : undefined}>
      <Field type="checkbox" as={Input} {...field} {...props} />
      {children}
    </StyledLabel>
  )
}

Checkbox.propTypes = {
  children: string.isRequired,
  name: string.isRequired,
  disabled: bool,
}

Checkbox.defaultProps = {
  disabled: false,
}

export default Checkbox
