// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'
import Icon from '../../../Icon'

export const inputStyles = css`
  display: block;
  padding: ${(p) => p.theme.tokens.spacing04};
  padding-left: ${(p) => p.$hasIcon && p.theme.tokens.spacing08};
  padding-right: ${(p) => p.$handleIconAtEnd && p.theme.tokens.spacing09};
  border: 1px solid ${(p) => p.theme.colorBorderInput};
  margin-top: ${(p) => p.theme.tokens.spacing01};
  outline: none;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-family: 'Source Sans Pro', sans-serif;
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  color: ${(p) => p.theme.colorTextInputLabel};
  background-color: ${(p) => p.theme.colorBackgroundInput};

  ::placeholder,
  ::-webkit-input-placeholder,
  :-ms-input-placeholder {
    color: ${(p) => p.theme.colorTextInputPlaceholder};
  }
  ::-webkit-calendar-picker-indicator {
    cursor: pointer;
    filter: ${(p) =>
      p.theme.name === 'default' ? 'invert(0.25)' : 'invert(0.8)'};
  }

  &:focus,
  &.invalid {
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    padding-left: calc(${(p) => p.$hasIcon && p.theme.tokens.spacing08} - 1px);
    padding-right: ${(p) => p.$handleIconAtEnd && p.theme.tokens.spacing09};
    border: 2px solid ${(p) => p.theme.colorBorderInputFocus};
  }

  &.invalid {
    border: 2px solid ${(p) => p.theme.colorBorderInputError};
  }

  ${(p) =>
    p.disabled
      ? css`
          background-color: ${(p) => p.theme.colorBackgroundInputDisabled};
          border-color: ${(p) => p.theme.colorBorderInputDisabled};
          cursor: not-allowed;
          color: ${(p) => p.theme.colorTextInputDisabled};
        `
      : null}

  ${(p) => {
    let width
    switch (p.size) {
      case 'xs':
        width = '5rem'
        break

      case 's':
        width = '10rem'
        break

      case 'm':
        width = '20rem'
        break

      case 'l':
        width = '30rem'
        break

      case 'xl':
        width = '46rem'
        break

      case 'fullWidth':
        width = '100%'
        break

      default:
        console.warn(
          `invalid size '${p.size}' provided. the supported values are xs, s, m, l, xl and fullWidth`,
        )
    }

    return css`
      width: ${width};
    `
  }}
`

export const StyledInput = styled.input`
  ${inputStyles}
`

export const Wrapper = styled.div`
  position: relative;
`

export const StyledIcon = styled(Icon)`
  position: absolute;
  fill: ${(p) => p.theme.colorIconInput};
  margin: auto 0 auto ${(p) => p.theme.tokens.spacing04};
  cursor: text;
  height: 100%;
`

export const StyledButton = styled.button`
  background: none;
  border: none;
  bottom: 2px;
  cursor: pointer;
  padding: 0 ${(p) => p.theme.tokens.spacing04} 0;
  position: absolute;
  right: 2px;
  top: 2px;

  svg {
    fill: ${(p) => p.theme.colorIconInput};
  }
`
