// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { FieldLabel } from '../index'
import Button from '../../Button'
import IconCheck from '../../Icon/check.svg'
import Input from './Input'
import TextInput from './index'

const textInputStory = {
  title: 'Components/Form/TextInput',
  parameters: {
    componentSubtitle:
      'Form component to work with text input. Batteries included (label, input & feedback).',
  },
  component: TextInput,
}

export default textInputStory

export const Intro = () => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Required'),
  })

  return (
    <>
      <Formik
        validationSchema={validationSchema}
        initialValues={{}}
        onSubmit={() => {}}
      >
        <Form>
          <TextInput name="name">
            <FieldLabel
              label="My input"
              small="Min two, max fifty characters"
            />
          </TextInput>
        </Form>
      </Formik>
    </>
  )
}

export const Placeholder = () => {
  return (
    <>
      <Formik initialValues={{}} onSubmit={() => {}}>
        <Form>
          <TextInput name="placeholder" placeholder="I am a placeholder">
            Placeholder
          </TextInput>
        </Form>
      </Formik>
    </>
  )
}

export const showError = () => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Required'),
  })

  return (
    <>
      <p>
        Useful if you want to override the default validation logic. In this
        example we only trigger the error if the form has been submitted.
      </p>

      <Formik
        validationSchema={validationSchema}
        initialValues={{}}
        onSubmit={() => {}}
      >
        {({ errors, submitCount }) => (
          <Form>
            <TextInput name="name" showError={errors.name && submitCount > 0}>
              My input
            </TextInput>

            <Button type="submit">Submit</Button>
          </Form>
        )}
      </Formik>
    </>
  )
}

export const Size = () => {
  return (
    <>
      <Formik initialValues={{}} onSubmit={() => {}}>
        <Form>
          <TextInput name="xs" size="xs">
            Extra small
          </TextInput>
          <TextInput name="s" size="s">
            Small
          </TextInput>
          <TextInput name="m" size="m">
            Medium (default)
          </TextInput>
          <TextInput name="l" size="l">
            Large
          </TextInput>
          <TextInput name="xl" size="xl">
            Extra large
          </TextInput>
          <TextInput name="fullWidth" size="fullWidth">
            Full width
          </TextInput>
        </Form>
      </Formik>
    </>
  )
}

export const Disabled = () => {
  return (
    <>
      <Formik initialValues={{ street: 'My street 007' }} onSubmit={() => {}}>
        <Form>
          <TextInput name="disabled" disabled>
            Name
          </TextInput>

          <TextInput name="street" disabled>
            Street
          </TextInput>
        </Form>
      </Formik>
    </>
  )
}

export const DateType = () => {
  return (
    <>
      <Formik initialValues={{}}>
        {() => (
          <TextInput name="date" type="date" placeholder="yyyy-mm-dd">
            Enter a date
          </TextInput>
        )}
      </Formik>

      <p style={{ marginTop: '1rem' }}>
        Browsers that don't support input type date require at least the{' '}
        <code>placeholder</code> attribute. You probably want to add additional
        error checking.
        <br />
        Because this depends on the project, it is not included in the design
        system.
      </p>
    </>
  )
}

export const withIcon = () => {
  return (
    <Formik initialValues={{}} onSubmit={() => {}}>
      <Form>
        <TextInput name="withIcon" icon={IconCheck} placeholder="placeholder">
          Input with indication icon
        </TextInput>
      </Form>
    </Formik>
  )
}

export const withIconAtEnd = () => {
  return (
    <Formik initialValues={{}} onSubmit={() => {}}>
      <Form>
        <TextInput
          name="name"
          placeholder="placeholder"
          handleIconAtEnd={() => {}}
          iconAtEnd={IconCheck}
        >
          Input with actionable icon at the end
        </TextInput>
      </Form>
    </Formik>
  )
}

export const TextareaType = () => {
  const validationSchema = Yup.object().shape({
    multiline: Yup.string().required('Required'),
  })

  return (
    <Formik
      initialValues={{}}
      validationSchema={validationSchema}
      onSubmit={() => {}}
    >
      <Form>
        <TextInput type="textarea" name="multiline">
          Required multiline input
        </TextInput>
      </Form>
    </Formik>
  )
}

export const InputComponent = () => {
  return (
    <>
      <p>
        Not using Formik? Importing <code>Input</code> directly gives you access
        to the text input without Formik integration. You can import the
        component in React using the following code:
      </p>
      <pre>
        <code>import SingleTextInput from '@commonground/design-system'</code>
      </pre>

      <label>
        Label
        <Input name="component" />
      </label>
    </>
  )
}
