// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Formik, Form } from 'formik'

import { Fieldset } from '../index'
import Radio from './index'

const radioStory = {
  title: 'Components/Form/Radio',
  parameters: {
    componentSubtitle:
      'Form component to work with radio buttons. Batteries included (label & input).',
  },
  component: Radio,
}

export default radioStory

export const Intro = () => {
  return (
    <>
      <Formik initialValues={{ areYouOk: 'yes' }} onSubmit={() => {}}>
        <Form>
          <Radio.Group label="Are you ok?">
            <Radio name="areYouOk" value="yes">
              Yes
            </Radio>
            <Radio name="areYouOk" value="no">
              No
            </Radio>
          </Radio.Group>
        </Form>
      </Formik>
    </>
  )
}

export const disabled = () => {
  return (
    <>
      <Formik
        initialValues={{ areYouOk: 'yes', pizza: 'maybe' }}
        onSubmit={() => {}}
      >
        <Form>
          <Fieldset>
            <Radio.Group label="Are you ok?">
              <Radio name="areYouOk" value="yes" disabled>
                Yes
              </Radio>
              <Radio name="areYouOk" value="no" disabled>
                No
              </Radio>
            </Radio.Group>
          </Fieldset>

          <Fieldset>
            <Radio.Group label="Do you like pizza?">
              <Radio name="pizza" value="yes">
                Yes
              </Radio>
              <Radio name="pizza" value="no">
                No
              </Radio>
              <Radio name="pizza" value="maybe" disabled>
                Maybe
              </Radio>
            </Radio.Group>
          </Fieldset>
        </Form>
      </Formik>
    </>
  )
}
