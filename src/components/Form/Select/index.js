// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { Field, useField } from 'formik'
import React from 'react'
import { bool, oneOf, oneOfType, string, shape } from 'prop-types'
import { FieldLabel } from '../index'
import { StyledLabel } from '../TextInput/index.styles'
import ErrorMessage from '../ErrorMessage'
import SelectComponent from './SelectComponent'
import SelectFormik from './SelectFormik'

const Select = ({ children, showError, ...props }) => {
  const [field, meta] = useField(props)
  const hasError =
    typeof showError === 'undefined' ? meta.error && meta.touched : showError
  const { disabled } = props

  // we extract the onChange property, because we deal with it
  // as part of the InputFormik component
  const { onChange, ...otherFieldOptions } = field

  return (
    <>
      <StyledLabel disabled={disabled ? true : undefined}>
        {children}
        <Field
          component={SelectFormik}
          disabled={disabled ? true : undefined}
          {...otherFieldOptions}
          {...props}
          className={hasError ? 'invalid' : null}
        />
      </StyledLabel>
      {hasError ? (
        <ErrorMessage data-testid={`error-${field.name}`}>
          {meta.error}
        </ErrorMessage>
      ) : null}
    </>
  )
}

Select.propTypes = {
  showError: bool,
  children: oneOfType([string, shape({ type: oneOf([FieldLabel]) })]),
  name: string.isRequired,
  size: oneOf(['xs', 's', 'm', 'l', 'xl']),
  disabled: bool,
  isMulti: bool,
}

Select.defaultProps = {
  size: 'm',
  disabled: false,
  isMulti: false,
}

export { Select, SelectFormik, SelectComponent }
