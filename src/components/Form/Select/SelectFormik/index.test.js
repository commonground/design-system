// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useRef } from 'react'
import { render, act, fireEvent } from '@testing-library/react'
import selectEvent from 'react-select-event'
import { Formik, Form, Field } from 'formik'
import TestThemeProvider from '../../../../themes/TestThemeProvider'
import SelectFormik from './index'

test('SelectFormik should function as Formik Field COMPONENT', async () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { selection: 'three' }

  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
          return (
            <Form>
              <label htmlFor="selection">Choose</label>
              <Field
                component={SelectFormik}
                options={options}
                name="selection"
                inputId="selection"
              />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('three')

  await selectEvent.select(getByLabelText('Choose'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

test('SelectFormik should accept objects as values', async () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const initialValues = { selection: options[2].value }

  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection.english
          return (
            <Form>
              <label htmlFor="selection">Choose</label>
              <Field
                component={SelectFormik}
                options={options}
                name="selection"
                inputId="selection"
              />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('three')

  await selectEvent.select(getByLabelText('Choose'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

test('SelectFormik should accept objects as values while accepting multiple values', async () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const initialValues = { selection: [options[2].value] }

  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
            .map((select) => select.english)
            .join(',')

          return (
            <Form>
              <label htmlFor="selection">Choose</label>
              <Field
                component={SelectFormik}
                options={options}
                name="selection"
                inputId="selection"
                isMulti
              />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('three')

  await selectEvent.select(getByLabelText('Choose'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('three,two')
})

test('SelectFormik should function as Formik Field COMPONENT and accept multiple values', async () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { selection: [options[1].value] }

  const { getByTestId, getByLabelText } = render(
    <Formik initialValues={initialValues}>
      {(formikProps) => {
        const selectionAsText = formikProps.values.selection.join(',')

        return (
          <TestThemeProvider>
            <Form>
              <label htmlFor="selection">Choose</label>
              <Field
                component={SelectFormik}
                options={options}
                name="selection"
                inputId="selection"
                isMulti
              />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          </TestThemeProvider>
        )
      }}
    </Formik>,
  )

  await selectEvent.select(getByLabelText('Choose'), 'Tres')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two,three')

  await act(async () => {
    await selectEvent.clearAll(getByLabelText('Choose'))
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('')
})

test('SelectFormik should function as Formik Field CHILD and pass a ref', async () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { selection: '' }

  const TestForm = () => {
    const select = useRef()

    return (
      <TestThemeProvider>
        <Formik initialValues={initialValues}>
          {(formikProps) => {
            const selectionAsText = formikProps.values.selection
            return (
              <Form
                data-testid="form"
                onReset={() => select.current.clearValue()}
              >
                <label htmlFor="selection">Choose</label>
                <Field name="selection">
                  {(props) => (
                    <SelectFormik
                      ref={select}
                      {...props}
                      options={options}
                      inputId="selection"
                    />
                  )}
                </Field>
                <span data-testid="parsed-selection">{selectionAsText}</span>
              </Form>
            )
          }}
        </Formik>
      </TestThemeProvider>
    )
  }

  const { getByTestId, getByLabelText } = render(<TestForm />)

  expect(getByTestId('parsed-selection')).toHaveTextContent('')

  await selectEvent.select(getByLabelText('Choose'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')

  await act(async () => {
    fireEvent.reset(getByTestId('form'))
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('')
})

test('SelectFormik should function when passed a non-standard option object', async () => {
  const initialValues = { selection: '' }

  const nonDefaultOptions = [
    { id: 1, name: 'Option one' },
    { id: 2, name: 'Option two' },
    { id: 3, name: 'Option three' },
  ]

  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
          return (
            <Form data-testid="form">
              <label htmlFor="selection">Choose</label>
              <Field name="selection">
                {(props) => (
                  <SelectFormik
                    {...props}
                    options={nonDefaultOptions}
                    getOptionValue={(option) => option.id}
                    getOptionLabel={(option) => option.name}
                    inputId="selection"
                  />
                )}
              </Field>
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('')

  await selectEvent.select(getByLabelText('Choose'), 'Option two')

  expect(getByTestId('parsed-selection')).toHaveTextContent('2')
})
