// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { css } from 'styled-components'

const style = (p) =>
  p.disabled
    ? css`
        cursor: not-allowed;
        color: ${p.theme.colorTextButtonSecondaryDisabled};
        background-color: ${p.theme.colorBackgroundButtonSecondaryDisabled};

        &:hover,
        &:focus {
          color: ${p.theme.colorTextButtonSecondaryDisabled};
          background-color: ${p.theme.colorBackgroundButtonSecondaryDisabled};
        }
      `
    : css`
        color: ${p.theme.colorTextButtonSecondary};
        background-color: ${p.theme.colorBackgroundButtonSecondary};

        &:hover,
        &:focus {
          color: ${p.theme.colorTextButtonSecondary};
          background: ${p.theme.colorBackgroundButtonSecondaryHover};
        }
      `

export default style
