// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { useState, useRef, useEffect } from 'react'
import { arrayOf, shape, string } from 'prop-types'
import IconFlippingChevron from '../IconFlippingChevron'
import IconExternalLink from './IconExternalLink'
import IconGitLab from './IconGitLab'
import {
  DomainListDesktop,
  DropdownButton,
  DropdownList,
  ExternalSiteIcon,
  GitLabIcon,
  GitLabLink,
  IntroText,
  Label,
  ListItem,
  ListItemDesktop,
  Navigation,
} from './index.styles'

const defaultDomains = [
  {
    domain: 'CommonGround.nl',
    description: 'Community voor Samen Organiseren',
    to: 'https://commonground.nl',
  },
  {
    domain: 'Componenten­catalogus',
    description:
      'Overzicht van herbruikbare software binnen de Nederlandse gemeenten',
    to: 'https://componentencatalogus.commonground.nl',
  },
  {
    domain: 'NLX',
    description:
      'Wissel snel, veilig en AVG-proof gegevens uit tussen overheidsorganisaties',
    to: 'https://nlx.io',
  },
  {
    domain: 'Haven',
    description:
      'Gestandaardiseerde infrastructuur voor het aanbieden en beheren van applicaties',
    to: 'https://haven.commonground.nl',
  },
  {
    domain: 'Developer Overheid',
    description:
      'Eén centrale plek voor de developer die voor of met de overheid ontwikkelt',
    to: 'https://developer.overheid.nl',
  },
]

const DomainNavigation = ({
  activeDomain,
  ariaLabel,
  domains,
  dropdownLabel,
  gitLabLink,
}) => {
  const [isOpen, setOpen] = useState(false)

  const listRef = useRef()
  const dropdownRef = useRef()

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        !(
          listRef.current?.contains(event.target) ||
          dropdownRef.current?.contains(event.target)
        )
      ) {
        setOpen(false)
      }
    }
    document.addEventListener('mousedown', handleClickOutside)

    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [listRef])

  return (
    <>
      <Navigation aria-label={ariaLabel}>
        <IntroText>
          Onderdeel van{` `}
          <br />
          Common Ground
        </IntroText>

        <DropdownButton
          aria-expanded={isOpen}
          onClick={() => setOpen(!isOpen)}
          onKeyDown={(e) => e.key === 'Enter' && setOpen(!isOpen)}
          ref={dropdownRef}
          role="button"
          tabIndex="0"
        >
          {dropdownLabel}
          <IconFlippingChevron flipHorizontal={isOpen} />
        </DropdownButton>

        <DomainListDesktop>
          {domains.map((option) => {
            const isSelected = option.domain === activeDomain
            return (
              <ListItemDesktop
                href={option.to}
                isSelected={isSelected}
                disabled={isSelected}
                key={option.domain}
                rel="noreferrer"
                tabIndex={isSelected ? -1 : 0}
                target="_blank"
              >
                <span>{option.domain}</span>
                <span>{option.domain}</span>
              </ListItemDesktop>
            )
          })}
        </DomainListDesktop>

        {gitLabLink && (
          <GitLabLink href={gitLabLink} target="_blank" rel="noreferrer">
            <GitLabIcon as={IconGitLab} />
            <div>
              <span>GitLab</span>
              <span>GitLab</span>
            </div>
          </GitLabLink>
        )}

        {isOpen && (
          <DropdownList ref={listRef}>
            {domains.map((option) => {
              const isSelected = option.domain === activeDomain
              return (
                <ListItem isSelected={isSelected} key={option.domain}>
                  {!isSelected ? (
                    <a
                      href={option.to}
                      onClick={() => setOpen((isOpen) => false)}
                      rel="noreferrer"
                      tabIndex={0}
                      target="_blank"
                    >
                      <Label>
                        {option.domain}
                        {option.description && (
                          <span>{option.description}</span>
                        )}
                      </Label>

                      <ExternalSiteIcon as={IconExternalLink} inline />
                    </a>
                  ) : (
                    <span
                      onClick={() => setOpen((isOpen) => false)}
                      rel="noreferrer"
                      tabIndex={-1}
                    >
                      <Label isActive={true}>
                        {option.domain}
                        {option.description && (
                          <span>{option.description}</span>
                        )}
                      </Label>
                    </span>
                  )}
                </ListItem>
              )
            })}
          </DropdownList>
        )}
      </Navigation>
    </>
  )
}

DomainNavigation.defaultProps = {
  ariaLabel: 'Domain navigatie',
  dropdownLabel: 'CG Producten',
  domains: defaultDomains,
}

DomainNavigation.propTypes = {
  activeDomain: string.isRequired,
  ariaLabel: string,
  domains: arrayOf(
    shape({
      domain: string.isRequired,
      description: string.isRequired,
      to: string.isRequired,
    }),
  ),
  dropdownLabel: string,
  gitLabLink: string,
}

export default DomainNavigation
