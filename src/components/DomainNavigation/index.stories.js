// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import DomainNavigation from './index'

const DomainNavigationStory = {
  title: 'Components/DomainNavigation',
  description: 'Navigation leading to NLX related domains',
  parameters: {
    componentSubtitle: 'Responsive, single level navigation.',
  },
  component: DomainNavigation,
}

export default DomainNavigationStory

export const Default = () => (
  <DomainNavigation
    activeDomain="NLX"
    gitLabLink="https://gitlab.com/commonground/core/design-system"
  />
)

export const WithCustomDomains = () => {
  const navItems = [
    {
      domain: 'Storybook',
      description:
        'The Common Ground (CG) Design System is een herbruikbare componenten bibliotheek.',
      to: '/',
    },
    {
      domain: 'Link 2',
      description: '',
      to: '/',
    },
    {
      domain: 'Link 3',
      description: '',
      to: '/',
    },
  ]

  return (
    <DomainNavigation
      activeDomain="Storybook"
      gitLabLink="https://gitlab.com/commonground/core/design-system"
      domains={navItems}
    />
  )
}
