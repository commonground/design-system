// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React from 'react'
import * as Yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, FormProvider } from 'react-hook-form'
import IconCheck from '../../Icon/check.svg'
import { FieldLabel } from '../../Form/index'
import Button from '../../Button'
import Input from '../../Form/TextInput/Input'
import TextInput from './index.js'

const textInputStory = {
  title: 'Components/ReactHookForm/TextInput',
  parameters: {
    componentSubtitle:
      'React Hook Form component to work with text input. Batteries included (label, input & feedback).',
  },
  component: TextInput,
}

export default textInputStory

const schema = Yup.object().shape({
  name: Yup.string()
    .required('Required')
    .min(2, 'Too Short!')
    .max(50, 'Too Long!'),
})

export const Intro = () => {
  const methods = useForm({
    mode: 'onTouched',
    defaultValues: {
      name: '',
    },
    resolver: yupResolver(schema),
  })

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit((fieldValues) => fieldValues)}>
          <TextInput name="name">
            <FieldLabel
              label="My input"
              small="Min two, max fifty characters"
            />
          </TextInput>
        </form>
      </FormProvider>
    </>
  )
}

export const Placeholder = () => {
  const methods = useForm({
    defaultValues: {
      placeholder: '',
    },
  })

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(() => {})}>
          <TextInput name="placeholder" placeholder="I am a placeholder">
            Placeholder
          </TextInput>
        </form>
      </FormProvider>
    </>
  )
}

export const ShowError = () => {
  const methods = useForm({
    mode: 'onSubmit',
    defaultValues: {
      name: '',
    },
    resolver: yupResolver(schema),
  })

  const formState = methods.formState

  return (
    <>
      <p>
        Useful if you want to override the default validation logic. In this
        example we only trigger the error if the form has been submitted.
      </p>

      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(() => {})}>
          <TextInput
            name="name"
            showError={
              !!formState.errors.name?.message && formState.submitCount > 0
            }
          >
            My input
          </TextInput>

          <Button type="submit">Submit</Button>
        </form>
      </FormProvider>
    </>
  )
}

export const Size = () => {
  const methods = useForm({
    defaultValues: {
      xs: '',
      s: '',
      m: '',
      l: '',
      xl: '',
      fullWidth: '',
    },
  })
  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(() => {})}>
          <TextInput name="xs" size="xs">
            Extra small
          </TextInput>
          <TextInput name="s" size="s">
            Small
          </TextInput>
          <TextInput name="m" size="m">
            Medium (default)
          </TextInput>
          <TextInput name="l" size="l">
            Large
          </TextInput>
          <TextInput name="xl" size="xl">
            Extra large
          </TextInput>
          <TextInput name="fullWidth" size="fullWidth">
            Full width
          </TextInput>
        </form>
      </FormProvider>
    </>
  )
}

export const Disabled = () => {
  const methods = useForm({
    defaultValues: {
      name: '',
      street: 'My street 007',
    },
  })
  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(() => {})}>
          <TextInput name="disabled" disabled>
            Name
          </TextInput>

          <TextInput name="street" disabled>
            Street
          </TextInput>
        </form>
      </FormProvider>
    </>
  )
}

export const DateType = () => {
  const methods = useForm({
    defaultValues: {
      date: '',
    },
  })
  return (
    <>
      <FormProvider {...methods}>
        <TextInput name="date" type="date" placeholder="yyyy-mm-dd">
          Enter a date
        </TextInput>
      </FormProvider>
    </>
  )
}

export const WithIcon = () => {
  const methods = useForm({
    defaultValues: {
      withIcon: '',
    },
  })
  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <TextInput name="withIcon" icon={IconCheck} placeholder="placeholder">
          Input with indication icon
        </TextInput>
      </form>
    </FormProvider>
  )
}

export const WithIconAtEnd = () => {
  const methods = useForm({
    defaultValues: {
      name: '',
    },
  })
  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit((fieldValues) => fieldValues)}>
        <TextInput
          name="name"
          placeholder="placeholder"
          handleIconAtEnd={() => {}}
          iconAtEnd={IconCheck}
        >
          Input with actionable icon at the end
        </TextInput>
      </form>
    </FormProvider>
  )
}

export const TextareaType = () => {
  const methods = useForm({
    mode: 'onBlur',
    defaultValues: {
      multiline: '',
    },
    resolver: yupResolver(
      Yup.object().shape({
        multiline: Yup.string().required('Required'),
      }),
    ),
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <TextInput type="textarea" name="multiline">
          Required multiline input
        </TextInput>
      </form>
    </FormProvider>
  )
}

export const InputComponent = () => {
  return (
    <>
      <p>
        Not using React Hook Form? Importing <code>Input</code> directly gives
        you access to the text input without React Hook Form integration. You
        can import the component in React using the following code:
      </p>
      <pre>
        <code>import SingleTextInput from '@commonground/design-system'</code>
      </pre>

      <label>
        Label
        <Input name="component" />
      </label>
    </>
  )
}
