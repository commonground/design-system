// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { bool, string } from 'prop-types'
import { useFormContext } from 'react-hook-form'
import Input from './Input'
import { StyledLabel } from './index.styles'

const Checkbox = ({ children, ...props }) => {
  const { register } = useFormContext()
  const { disabled, name } = props

  return (
    <StyledLabel disabled={disabled}>
      <Input type="checkbox" {...register(name)} {...props} />
      {children}
    </StyledLabel>
  )
}

Checkbox.propTypes = {
  children: string.isRequired,
  name: string.isRequired,
  disabled: bool,
  value: string,
}

Checkbox.defaultProps = {
  disabled: false,
}

export default Checkbox
