// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import ReactSelect from 'react-select'
import styled, { css } from 'styled-components'

const StyledReactSelect = styled(ReactSelect)`
  margin-top: ${(p) => p.theme.tokens.spacing01};

  .ReactSelect__control {
    border-radius: 0;
    background-color: ${(p) => p.theme.colorBackgroundSelect};
    padding: ${(p) => p.theme.tokens.spacing02};
    border-color: ${(p) => p.theme.colorBorderSelect};
    color: ${(p) => p.theme.colorText};

    &:hover {
      border-color: ${(p) => p.theme.colorBorderSelect};
    }

    &--menu-is-open,
    &--menu-is-open:hover,
    &--is-focused,
    &--is-focused:hover {
      border-color: ${(p) => p.theme.colorBorderSelectFocus};
      box-shadow: 0 0 0 1px ${(p) => p.theme.colorBorderSelectFocus};
    }

    &--is-disabled,
    &--is-disabled:hover {
      background-color: ${(p) => p.theme.colorBackgroundSelectDisabled};
      pointer-events: all;
      cursor: not-allowed;
      border-color: ${(p) => p.theme.colorBorderSelectDisabled};
      box-shadow: none;
    }
  }

  .ReactSelect__value-container {
    padding-top: ${(p) => p.theme.tokens.spacing02};
    padding-bottom: ${(p) => p.theme.tokens.spacing02};
  }

  .ReactSelect__multi-value {
    background-color: ${(p) => p.theme.colorBackgroundSelectOptionSelect};
  }

  .ReactSelect__multi-value__label {
    color: ${(p) => p.theme.colorText};
  }

  .ReactSelect__single-value {
    color: ${(p) => p.theme.colorText};
  }

  .ReactSelect__menu {
    background-color: ${(p) => p.theme.colorBackgroundSelect};
    border-radius: 0;
  }

  .ReactSelect__option {
    &--is-focused {
      background-color: ${(p) => p.theme.colorBackgroundSelectOptionHover};
    }

    &--is-selected {
      color: ${(p) => p.theme.colorText};
      background-color: ${(p) => p.theme.colorBackgroundSelectOptionSelect};
    }

    &--is-disabled {
      color: ${(p) => p.theme.colorTextSelectDisabled};
      cursor: not-allowed;
    }
  }

  .ReactSelect__indicator-separator {
    background-color: ${(p) => p.theme.colorBackgroundSelectSeparator};
  }

  &.invalid {
    .ReactSelect__control {
      border-color: ${(p) => p.theme.colorBorderSelectError};
      box-shadow: 0 0 0 1px ${(p) => p.theme.colorBorderSelectError};
    }
  }

  .ReactSelect__placeholder {
    color: ${(p) => p.theme.colorTextSelectPlaceholder};
  }

  ${(p) => {
    let width
    switch (p.size) {
      case 'xs':
        width = '5rem'
        break

      case 's':
        width = '10rem'
        break

      case 'm':
        width = '20rem'
        break

      case 'l':
        width = '30rem'
        break

      case 'xl':
        width = '46rem'
        break

      default:
        console.warn(
          `invalid size '${p.size}' provided. the supported values are xs, s, m, l and xl`,
        )
    }

    return css`
      width: ${width};
    `
  }}
`

export default StyledReactSelect
