// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter, NavLink } from 'react-router-dom'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import { runAxe } from '../../../test-helpers/axe'
import DesktopNav from './index'

const navItems = [
  {
    name: 'Home',
    to: '/',
    'data-testid': 'link-homepage',
  },
  {
    name: 'Producten',
    to: '/producten',
    'data-testid': 'link-products',
  },
  {
    name: 'Componenten',
    to: '/componenten',
    'data-testid': 'link-components',
  },
  {
    name: 'Principes',
    to: '/principes',
    'data-testid': 'link-principes',
  },
]

describe('the Primary Desktop Navigation', () => {
  it('should render expected number of nav items', () => {
    const { getAllByTestId } = render(
      <MemoryRouter>
        <TestThemeProvider>
          <DesktopNav
            items={navItems}
            LinkComponent={NavLink}
            ariaLabel="test"
          />
        </TestThemeProvider>
      </MemoryRouter>,
    )

    expect(getAllByTestId(/link-/)).toHaveLength(4)
  })

  it('should be accessible', async () => {
    const { container } = render(
      <MemoryRouter>
        <TestThemeProvider>
          <DesktopNav
            items={navItems}
            LinkComponent={NavLink}
            ariaLabel="test"
          />
        </TestThemeProvider>
      </MemoryRouter>,
    )
    const results = await runAxe(container)

    expect(results).toHaveNoA11yViolations()
  })
})
