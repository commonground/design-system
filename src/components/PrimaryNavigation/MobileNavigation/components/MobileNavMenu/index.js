// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { array, bool, func, elementType, string } from 'prop-types'

import MobileNavItem from '../MobileNavItem'
import MobileMoreButton from '../MobileMoreButton'

import { MobileNavWrapper } from './index.styles'

const MobileNavMenu = ({
  ariaLabel,
  LinkComponent,
  items,
  hasMoreItems,
  onClick,
  isMoreActive,
  mobileMoreText,
}) => (
  <MobileNavWrapper role="navigation" aria-label={ariaLabel}>
    {items.map((item) => (
      <MobileNavItem
        key={item.name}
        to={item.to}
        LinkComponent={LinkComponent}
        {...item}
      />
    ))}
    {hasMoreItems ? (
      <MobileMoreButton
        onClick={onClick}
        className={isMoreActive && 'active'}
        mobileMoreText={mobileMoreText}
      />
    ) : null}
  </MobileNavWrapper>
)

MobileNavMenu.propTypes = {
  ariaLabel: string,
  LinkComponent: elementType.isRequired,
  items: array.isRequired,
  hasMoreItems: bool.isRequired,
  onClick: func.isRequired,
  isMoreActive: bool.isRequired,
  mobileMoreText: string.isRequired,
}

export default MobileNavMenu
