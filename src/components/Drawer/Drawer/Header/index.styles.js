// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import styled from 'styled-components'

export const StyledHeader = styled.div`
  display: flex;
  align-items: flex-start;
`

export const StyledTitle = styled.h1`
  flex: 1;
  margin-bottom: 0;
  word-break: break-word;
`
