## [21.7.1](https://gitlab.com/commonground/core/design-system/compare/v21.7.0...v21.7.1) (2023-09-06)


### Bug Fixes

* **form:** add unknown DOM props as transient ([a60714c](https://gitlab.com/commonground/core/design-system/commit/a60714cd22e81f27622e3c4e885a2c567c07f27a))

# [21.7.0](https://gitlab.com/commonground/core/design-system/compare/v21.6.0...v21.7.0) (2023-09-06)


### Features

* **form:** make text input icon at the end more generic ([1bc9c13](https://gitlab.com/commonground/core/design-system/commit/1bc9c132d21aacf68c01ae7cc133edad1505f0b5))

# [21.6.0](https://gitlab.com/commonground/core/design-system/compare/v21.5.1...v21.6.0) (2023-07-18)


### Features

* **form:** add function prop type for icon to allow for compiled svg's ([cc88469](https://gitlab.com/commonground/core/design-system/commit/cc884697d3d850f13e5b201d5291ef9e48874560))

## [21.5.1](https://gitlab.com/commonground/core/design-system/compare/v21.5.0...v21.5.1) (2023-03-22)

# [21.5.0](https://gitlab.com/commonground/core/design-system/compare/v21.4.1...v21.5.0) (2023-03-21)


### Features

* enable using React v18 ([234256e](https://gitlab.com/commonground/core/design-system/commit/234256ea0f8d12ec7a6f6750e2994d4504ce2347))

## [21.4.1](https://gitlab.com/commonground/core/design-system/compare/v21.4.0...v21.4.1) (2023-03-20)

# [21.4.0](https://gitlab.com/commonground/core/design-system/compare/v21.3.1...v21.4.0) (2023-03-09)


### Features

* **form:** add Select with support for React Hook Form ([d167c5c](https://gitlab.com/commonground/core/design-system/commit/d167c5c18c3d447eecb0e47890b808ccb1f98d16))

## [21.3.1](https://gitlab.com/commonground/core/design-system/compare/v21.3.0...v21.3.1) (2023-02-28)

# [21.3.0](https://gitlab.com/commonground/core/design-system/compare/v21.2.3...v21.3.0) (2023-02-23)


### Features

* **form:** add Checkbox and Radio with support for React Hook Form ([7be7af9](https://gitlab.com/commonground/core/design-system/commit/7be7af960fe6de02db9264aca43ed7b5c1c3c477))

## [21.2.3](https://gitlab.com/commonground/core/design-system/compare/v21.2.2...v21.2.3) (2023-02-09)

## [21.2.2](https://gitlab.com/commonground/core/design-system/compare/v21.2.1...v21.2.2) (2023-02-08)

## [21.2.1](https://gitlab.com/commonground/core/design-system/compare/v21.2.0...v21.2.1) (2023-02-07)

# [21.2.0](https://gitlab.com/commonground/core/design-system/compare/v21.1.21...v21.2.0) (2023-01-19)


### Features

* **form:** add TextInput with support for React Hook Form ([29e8556](https://gitlab.com/commonground/core/design-system/commit/29e8556a5f791efd19c5f6bf771b9107801d5bdd))

## [21.1.21](https://gitlab.com/commonground/core/design-system/compare/v21.1.20...v21.1.21) (2023-01-19)

## [21.1.20](https://gitlab.com/commonground/core/design-system/compare/v21.1.19...v21.1.20) (2023-01-16)

## [21.1.19](https://gitlab.com/commonground/core/design-system/compare/v21.1.18...v21.1.19) (2023-01-06)

## [21.1.18](https://gitlab.com/commonground/core/design-system/compare/v21.1.17...v21.1.18) (2022-12-16)

## [21.1.17](https://gitlab.com/commonground/core/design-system/compare/v21.1.16...v21.1.17) (2022-12-05)

## [21.1.16](https://gitlab.com/commonground/core/design-system/compare/v21.1.15...v21.1.16) (2022-11-29)

## [21.1.15](https://gitlab.com/commonground/core/design-system/compare/v21.1.14...v21.1.15) (2022-11-22)

## [21.1.14](https://gitlab.com/commonground/core/design-system/compare/v21.1.13...v21.1.14) (2022-09-21)

## [21.1.13](https://gitlab.com/commonground/core/design-system/compare/v21.1.12...v21.1.13) (2022-08-30)

## [21.1.12](https://gitlab.com/commonground/core/design-system/compare/v21.1.11...v21.1.12) (2022-08-30)

## [21.1.11](https://gitlab.com/commonground/core/design-system/compare/v21.1.10...v21.1.11) (2022-08-18)

## [21.1.10](https://gitlab.com/commonground/core/design-system/compare/v21.1.9...v21.1.10) (2022-08-03)

## [21.1.9](https://gitlab.com/commonground/core/design-system/compare/v21.1.8...v21.1.9) (2022-07-21)

## [21.1.8](https://gitlab.com/commonground/core/design-system/compare/v21.1.7...v21.1.8) (2022-06-28)

## [21.1.7](https://gitlab.com/commonground/core/design-system/compare/v21.1.6...v21.1.7) (2022-06-22)

## [21.1.6](https://gitlab.com/commonground/core/design-system/compare/v21.1.5...v21.1.6) (2022-05-31)

## [21.1.5](https://gitlab.com/commonground/core/design-system/compare/v21.1.4...v21.1.5) (2022-04-15)

## [21.1.4](https://gitlab.com/commonground/core/design-system/compare/v21.1.3...v21.1.4) (2022-03-31)

## [21.1.3](https://gitlab.com/commonground/core/design-system/compare/v21.1.2...v21.1.3) (2022-03-24)


### Bug Fixes

* **collapsible:** use text color token for the content ([240feb2](https://gitlab.com/commonground/core/design-system/commit/240feb2d43879fe422dbe39dafe6a54c3cd15fe9))

## [21.1.2](https://gitlab.com/commonground/core/design-system/compare/v21.1.1...v21.1.2) (2022-03-22)

## [21.1.1](https://gitlab.com/commonground/core/design-system/compare/v21.1.0...v21.1.1) (2022-03-09)


### Bug Fixes

* **collapsible:** resolve accessibility violations ([bffff48](https://gitlab.com/commonground/core/design-system/commit/bffff48331fa132642a074e7fb1f166a37982c4b))
* **form:** resolve accessibility violations ([2d41353](https://gitlab.com/commonground/core/design-system/commit/2d41353bb8e1fb10ee98945cedd11cbaceb2f760))
* **primary-nav:** resolve accessibility violations ([60d1c51](https://gitlab.com/commonground/core/design-system/commit/60d1c5151e29d38b7262120c05342d1223d7f3a5))

# [21.1.0](https://gitlab.com/commonground/core/design-system/compare/v21.0.2...v21.1.0) (2022-03-07)


### Features

* **form:** improve styling of disabled Select options ([33336f7](https://gitlab.com/commonground/core/design-system/commit/33336f73d77e054096d7c4be416d560a5fd2615e))

## [21.0.2](https://gitlab.com/commonground/core/design-system/compare/v21.0.1...v21.0.2) (2022-02-01)

## [21.0.1](https://gitlab.com/commonground/core/design-system/compare/v21.0.0...v21.0.1) (2022-01-20)


### Bug Fixes

* **drawer:** ensure a long Drawer title does not push away the close button ([a757de6](https://gitlab.com/commonground/core/design-system/commit/a757de637a6448e6c10004e51973d661f623d4fd))

# [21.0.0](https://gitlab.com/commonground/core/design-system/compare/v20.0.0...v21.0.0) (2022-01-10)


### Build System

* upgrade Select v4 -> v5 ([2d6b420](https://gitlab.com/commonground/core/design-system/commit/2d6b420dee85ed0f2084d03446f80d563cc495d7)), closes [/react-select.com/upgrade#from-v4-to-v5](https://gitlab.com//react-select.com/upgrade/issues/from-v4-to-v5)


### BREAKING CHANGES

* see changelog of react-select

# [20.0.0](https://gitlab.com/commonground/core/design-system/compare/v19.0.10...v20.0.0) (2022-01-10)


### Build System

* upgrade React Router v5.x to v6.x ([d919a59](https://gitlab.com/commonground/core/design-system/commit/d919a59059239b6638d723d20ac7dd099d95371f))


### BREAKING CHANGES

* please upgrade React Router to v6 for your application

## [19.0.10](https://gitlab.com/commonground/core/design-system/compare/v19.0.9...v19.0.10) (2022-01-07)

## [19.0.9](https://gitlab.com/commonground/core/design-system/compare/v19.0.8...v19.0.9) (2022-01-07)

## [19.0.8](https://gitlab.com/commonground/core/design-system/compare/v19.0.7...v19.0.8) (2021-12-24)

## [19.0.7](https://gitlab.com/commonground/core/design-system/compare/v19.0.6...v19.0.7) (2021-11-17)

## [19.0.6](https://gitlab.com/commonground/core/design-system/compare/v19.0.5...v19.0.6) (2021-11-17)

## [19.0.5](https://gitlab.com/commonground/core/design-system/compare/v19.0.4...v19.0.5) (2021-10-29)


### Bug Fixes

* **collapsible:** remove redundant aria-label property ([631f4b0](https://gitlab.com/commonground/core/design-system/commit/631f4b096f2b3b9cc7d9d2b3777278cf44570854)), closes [/github.com/dequelabs/axe-core/blob/master/CHANGELOG.md#434-2021-10-22](https://gitlab.com//github.com/dequelabs/axe-core/blob/master/CHANGELOG.md/issues/434-2021-10-22)

## [19.0.4](https://gitlab.com/commonground/core/design-system/compare/v19.0.3...v19.0.4) (2021-10-20)

## [19.0.3](https://gitlab.com/commonground/core/design-system/compare/v19.0.2...v19.0.3) (2021-10-11)


### Bug Fixes

* **form:** missing svg on input clear ([1eae191](https://gitlab.com/commonground/core/design-system/commit/1eae19184f93caf6c4b68a6509030efed0478706))

## [19.0.2](https://gitlab.com/commonground/core/design-system/compare/v19.0.1...v19.0.2) (2021-10-11)

## [19.0.1](https://gitlab.com/commonground/core/design-system/compare/v19.0.0...v19.0.1) (2021-10-06)


### Bug Fixes

* **form:** resolve issue with clear text input button ([cb88380](https://gitlab.com/commonground/core/design-system/commit/cb883805714d2c238d08ee9f806f23b88ff89822))

# [19.0.0](https://gitlab.com/commonground/core/design-system/compare/v18.14.0...v19.0.0) (2021-10-05)


### Bug Fixes

* downgrade @storybook/preset-create-react-app v4.x -> 3.2.0 ([8137223](https://gitlab.com/commonground/core/design-system/commit/81372233fa2b519fa0f3755d96d13bb82fa16b5b))


### Features

* **form:** implement optional clear button for text input ([ce2d2fd](https://gitlab.com/commonground/core/design-system/commit/ce2d2fdf58097f1a6b79a8e7a74b63bf412a9593))
* remove support for React v16.x ([35023c1](https://gitlab.com/commonground/core/design-system/commit/35023c15c051e69c79a5e3bb15d159d2361285f2))


### BREAKING CHANGES

* please upgrade to React v17.x

# [18.14.0](https://gitlab.com/commonground/core/design-system/compare/v18.13.8...v18.14.0) (2021-10-05)


### Features

* **form:** implement optional clear button for text input ([fe339d8](https://gitlab.com/commonground/core/design-system/commit/fe339d802a43de7a197eba276206ece638c03427))

## [18.13.8](https://gitlab.com/commonground/core/design-system/compare/v18.13.7...v18.13.8) (2021-10-04)

## [18.13.7](https://gitlab.com/commonground/core/design-system/compare/v18.13.6...v18.13.7) (2021-10-04)

## [18.13.6](https://gitlab.com/commonground/core/design-system/compare/v18.13.5...v18.13.6) (2021-09-30)

## [18.13.5](https://gitlab.com/commonground/core/design-system/compare/v18.13.4...v18.13.5) (2021-09-30)

## [18.13.4](https://gitlab.com/commonground/core/design-system/compare/v18.13.3...v18.13.4) (2021-09-22)

## [18.13.3](https://gitlab.com/commonground/core/design-system/compare/v18.13.2...v18.13.3) (2021-09-15)

## [18.13.2](https://gitlab.com/commonground/core/design-system/compare/v18.13.1...v18.13.2) (2021-09-13)

## [18.13.1](https://gitlab.com/commonground/core/design-system/compare/v18.13.0...v18.13.1) (2021-09-10)

# [18.13.0](https://gitlab.com/commonground/core/design-system/compare/v18.12.6...v18.13.0) (2021-09-09)


### Bug Fixes

* **form:** resolve multi input regular value issue ([ba6852d](https://gitlab.com/commonground/core/design-system/commit/ba6852d70f42c4e9e2996b895a80bef8778cf325))


### Features

* **domain-nav:** remove link from active item ([94916f8](https://gitlab.com/commonground/core/design-system/commit/94916f8870f4642bc3c490099ccda91af31757d0))

## [18.12.6](https://gitlab.com/commonground/core/design-system/compare/v18.12.5...v18.12.6) (2021-09-06)

## [18.12.5](https://gitlab.com/commonground/core/design-system/compare/v18.12.4...v18.12.5) (2021-09-03)

## [18.12.4](https://gitlab.com/commonground/core/design-system/compare/v18.12.3...v18.12.4) (2021-08-30)

## [18.12.3](https://gitlab.com/commonground/core/design-system/compare/v18.12.2...v18.12.3) (2021-08-26)

## [18.12.2](https://gitlab.com/commonground/core/design-system/compare/v18.12.1...v18.12.2) (2021-08-25)

## [18.12.1](https://gitlab.com/commonground/core/design-system/compare/v18.12.0...v18.12.1) (2021-08-24)

# [18.12.0](https://gitlab.com/commonground/core/design-system/compare/v18.11.2...v18.12.0) (2021-08-24)


### Features

* **form:** add detached input component story ([d942cbf](https://gitlab.com/commonground/core/design-system/commit/d942cbf3dd6979453d0d8282fa70b380f82ca568))
* **form:** implement optional icon for text input ([246e19b](https://gitlab.com/commonground/core/design-system/commit/246e19b9eeb2efce813e4f65bbc0fc94a6366365))

## [18.11.2](https://gitlab.com/commonground/core/design-system/compare/v18.11.1...v18.11.2) (2021-08-16)

## [18.11.1](https://gitlab.com/commonground/core/design-system/compare/v18.11.0...v18.11.1) (2021-08-12)


### Bug Fixes

* **primary-nav:** make the ariaLabel property optional, since a default value is provided ([bd0032f](https://gitlab.com/commonground/core/design-system/commit/bd0032f72a6c547c3743c2a91c3ca1ad6bc8b4aa))
* **primary-nav:** replace common propType object with component specific propTypes ([1bbbabc](https://gitlab.com/commonground/core/design-system/commit/1bbbabcbebe55b4767b0e4cb77abbed619ad5a8a))

# [18.11.0](https://gitlab.com/commonground/core/design-system/compare/v18.10.10...v18.11.0) (2021-08-11)


### Bug Fixes

* **form:** add isMulti propType for Select ([eb24a1a](https://gitlab.com/commonground/core/design-system/commit/eb24a1ae31284180b3f03117e2f5fd9450220c28))
* **form:** prevent isMulti Select from resetting its value on form submit ([a94cc27](https://gitlab.com/commonground/core/design-system/commit/a94cc27661c9881ba54f46d7f4552bbfd0841d5a)), closes [nlx#1305](https://gitlab.com/nlx/issues/1305)


### Features

* **form:** add story for objects as values for Select in multi mode ([7e6d9d9](https://gitlab.com/commonground/core/design-system/commit/7e6d9d959ef5e9d6377afb58d2e9eff74fd7cf18))

## [18.10.10](https://gitlab.com/commonground/core/design-system/compare/v18.10.9...v18.10.10) (2021-08-11)

## [18.10.9](https://gitlab.com/commonground/core/design-system/compare/v18.10.8...v18.10.9) (2021-08-09)

## [18.10.8](https://gitlab.com/commonground/core/design-system/compare/v18.10.7...v18.10.8) (2021-08-03)


### Bug Fixes

* **form:** make safari date placeholder more apparant ([3eacb59](https://gitlab.com/commonground/core/design-system/commit/3eacb59cd181d3aba3d814e249b26fa8d05475ce))

## [18.10.7](https://gitlab.com/commonground/core/design-system/compare/v18.10.6...v18.10.7) (2021-07-29)

## [18.10.6](https://gitlab.com/commonground/core/design-system/compare/v18.10.5...v18.10.6) (2021-07-29)

## [18.10.5](https://gitlab.com/commonground/core/design-system/compare/v18.10.4...v18.10.5) (2021-07-21)

## [18.10.4](https://gitlab.com/commonground/core/design-system/compare/v18.10.3...v18.10.4) (2021-07-16)

## [18.10.3](https://gitlab.com/commonground/core/design-system/compare/v18.10.2...v18.10.3) (2021-07-09)

## [18.10.2](https://gitlab.com/commonground/core/design-system/compare/v18.10.1...v18.10.2) (2021-07-08)

## [18.10.1](https://gitlab.com/commonground/core/design-system/compare/v18.10.0...v18.10.1) (2021-07-05)


### Bug Fixes

* **collapsible:** svg takes in aria-label as title when available ([4cace0c](https://gitlab.com/commonground/core/design-system/commit/4cace0ce2648a05a9f8023b5f55f89872a45f45a))

# [18.10.0](https://gitlab.com/commonground/core/design-system/compare/v18.9.4...v18.10.0) (2021-07-05)


### Features

* **collapsible:** enable collapsing using keyboard ([61d2000](https://gitlab.com/commonground/core/design-system/commit/61d2000e0057c87efafa365e47ba0f3cf1c620b6))

## [18.9.4](https://gitlab.com/commonground/core/design-system/compare/v18.9.3...v18.9.4) (2021-07-05)

## [18.9.3](https://gitlab.com/commonground/core/design-system/compare/v18.9.2...v18.9.3) (2021-07-01)

## [18.9.2](https://gitlab.com/commonground/core/design-system/compare/v18.9.1...v18.9.2) (2021-06-29)

## [18.9.1](https://gitlab.com/commonground/core/design-system/compare/v18.9.0...v18.9.1) (2021-06-29)

# [18.9.0](https://gitlab.com/commonground/core/design-system/compare/v18.8.3...v18.9.0) (2021-06-28)


### Features

* **button:** add selected hover color ([34d3a3f](https://gitlab.com/commonground/core/design-system/commit/34d3a3f4cde883142dcb33d9e36a758153f3eb58))

## [18.8.3](https://gitlab.com/commonground/core/design-system/compare/v18.8.2...v18.8.3) (2021-06-21)


### Bug Fixes

* **domain-nav:** resolve issue for safari users ([3585fe2](https://gitlab.com/commonground/core/design-system/commit/3585fe2b862199374e11de614a94396cd839e09b))

## [18.8.2](https://gitlab.com/commonground/core/design-system/compare/v18.8.1...v18.8.2) (2021-06-21)


### Bug Fixes

* **domain-nav:** resolve issue with building svg ([5637855](https://gitlab.com/commonground/core/design-system/commit/5637855eff246994169aeef3f9259e479e1a59bb))

## [18.8.1](https://gitlab.com/commonground/core/design-system/compare/v18.8.0...v18.8.1) (2021-06-17)

# [18.8.0](https://gitlab.com/commonground/core/design-system/compare/v18.7.14...v18.8.0) (2021-06-16)


### Features

* **domain-nav:** introduce component ([11b0a87](https://gitlab.com/commonground/core/design-system/commit/11b0a878bd38407c3d987a999389570dc0f8b034))
* **primary-nav:** enable passing aria-label prop ([b288aab](https://gitlab.com/commonground/core/design-system/commit/b288aab0cbe6ba7e28200faf20dff91e109af63e))

## [18.7.14](https://gitlab.com/commonground/core/design-system/compare/v18.7.13...v18.7.14) (2021-06-16)

## [18.7.13](https://gitlab.com/commonground/core/design-system/compare/v18.7.12...v18.7.13) (2021-06-15)

## [18.7.12](https://gitlab.com/commonground/core/design-system/compare/v18.7.11...v18.7.12) (2021-06-14)

## [18.7.11](https://gitlab.com/commonground/core/design-system/compare/v18.7.10...v18.7.11) (2021-06-10)

## [18.7.10](https://gitlab.com/commonground/core/design-system/compare/v18.7.9...v18.7.10) (2021-06-07)

## [18.7.9](https://gitlab.com/commonground/core/design-system/compare/v18.7.8...v18.7.9) (2021-06-03)

## [18.7.8](https://gitlab.com/commonground/core/design-system/compare/v18.7.7...v18.7.8) (2021-05-11)

## [18.7.7](https://gitlab.com/commonground/core/design-system/compare/v18.7.6...v18.7.7) (2021-05-04)

## [18.7.6](https://gitlab.com/commonground/core/design-system/compare/v18.7.5...v18.7.6) (2021-05-03)

## [18.7.5](https://gitlab.com/commonground/core/design-system/compare/v18.7.4...v18.7.5) (2021-05-03)

## [18.7.4](https://gitlab.com/commonground/core/design-system/compare/v18.7.3...v18.7.4) (2021-04-27)

## [18.7.3](https://gitlab.com/commonground/core/design-system/compare/v18.7.2...v18.7.3) (2021-04-26)


### Bug Fixes

* after major release of addon-styled-components ([98f54eb](https://gitlab.com/commonground/core/design-system/commit/98f54ebeaa2a446076d51761477f2d30ec17433d))

## [18.7.2](https://gitlab.com/commonground/core/design-system/compare/v18.7.1...v18.7.2) (2021-04-20)

## [18.7.1](https://gitlab.com/commonground/core/design-system/compare/v18.7.0...v18.7.1) (2021-04-20)

# [18.7.0](https://gitlab.com/commonground/core/design-system/compare/v18.6.1...v18.7.0) (2021-04-15)


### Features

* **form:** enable passing complex objects as values to Select ([03b8287](https://gitlab.com/commonground/core/design-system/commit/03b8287c5fb4a2668ae5acd32e546350b80e399b))

## [18.6.1](https://gitlab.com/commonground/core/design-system/compare/v18.6.0...v18.6.1) (2021-04-14)

# [18.6.0](https://gitlab.com/commonground/core/design-system/compare/v18.5.1...v18.6.0) (2021-04-13)


### Features

* **form:** enable & style multi-option for Select ([bc331e1](https://gitlab.com/commonground/core/design-system/commit/bc331e10f906fd13ce9a648617f370cc0a201249))

## [18.5.1](https://gitlab.com/commonground/core/design-system/compare/v18.5.0...v18.5.1) (2021-04-09)

# [18.5.0](https://gitlab.com/commonground/core/design-system/compare/v18.4.4...v18.5.0) (2021-04-08)


### Bug Fixes

* **form:** max-width for labels ([ddcbc87](https://gitlab.com/commonground/core/design-system/commit/ddcbc8719eaaf4868e2bb497a0bc2b3c9e3fa5b4))


### Features

* **form:** textInput type textarea ([c08f3b3](https://gitlab.com/commonground/core/design-system/commit/c08f3b32f1d9755f7932bcdc39a995c0f27395e4))

## [18.4.4](https://gitlab.com/commonground/core/design-system/compare/v18.4.3...v18.4.4) (2021-03-25)


### Bug Fixes

* displaying additional svg in mobile nav menu ([5702f8e](https://gitlab.com/commonground/core/design-system/commit/5702f8ee52325fc85013b4ccefcd80f0f2ac9509))

## [18.4.3](https://gitlab.com/commonground/core/design-system/compare/v18.4.2...v18.4.3) (2021-03-23)

## [18.4.2](https://gitlab.com/commonground/core/design-system/compare/v18.4.1...v18.4.2) (2021-03-23)


### Bug Fixes

* resolve issue with publishing package ([136a987](https://gitlab.com/commonground/core/design-system/commit/136a9874601908e5f839f68f6f65aaf5fd48c3cd))

## [18.4.1](https://gitlab.com/commonground/core/design-system/compare/v18.4.0...v18.4.1) (2021-03-23)


### Bug Fixes

* **input:** error icon safari ([22fee0b](https://gitlab.com/commonground/core/design-system/commit/22fee0b60bf5caa8d5a892731a5d462157712261))

# [18.4.0](https://gitlab.com/commonground/core/design-system/compare/v18.3.2...v18.4.0) (2021-03-23)


### Bug Fixes

* **primarynav:** fouc desktop ([58be9e1](https://gitlab.com/commonground/core/design-system/commit/58be9e1f001de9d4ee2d8f1391c0d04ac5647445))


### Features

* **primarynav:** show light theme warning ([ed05f94](https://gitlab.com/commonground/core/design-system/commit/ed05f94c550c04a73f6bbe77ca33a0070ecc9531))

## [18.3.2](https://gitlab.com/commonground/core/design-system/compare/v18.3.1...v18.3.2) (2021-03-17)

## [18.3.1](https://gitlab.com/commonground/core/design-system/compare/v18.3.0...v18.3.1) (2021-03-09)


### Bug Fixes

* **form:** prop-types ([57adde9](https://gitlab.com/commonground/core/design-system/commit/57adde90e9e5c24564a77ccda7e0922b000aa60f))

# [18.3.0](https://gitlab.com/commonground/core/design-system/compare/v18.2.3...v18.3.0) (2021-03-09)


### Bug Fixes

* **input:** date type allowed and webkit icon styled ([3a5fb27](https://gitlab.com/commonground/core/design-system/commit/3a5fb27dd647d956d20438e97b6c831b59b59b2f))


### Features

* **form:** added FieldLabel component ([13404ba](https://gitlab.com/commonground/core/design-system/commit/13404badd4e9431767c4387f12a5a62f0a1c608d))

## [18.2.3](https://gitlab.com/commonground/core/design-system/compare/v18.2.2...v18.2.3) (2021-03-03)

## [18.2.2](https://gitlab.com/commonground/core/design-system/compare/v18.2.1...v18.2.2) (2021-03-03)

## [18.2.1](https://gitlab.com/commonground/core/design-system/compare/v18.2.0...v18.2.1) (2021-03-01)

# [18.2.0](https://gitlab.com/commonground/core/design-system/compare/v18.1.0...v18.2.0) (2021-02-25)


### Features

* **primarynav:** implement PrimaryNavigation component ([a3a301a](https://gitlab.com/commonground/core/design-system/commit/a3a301a774190226c2dd50625e0aca36c1980ec8))

# [18.1.0](https://gitlab.com/commonground/core/design-system/compare/v18.0.2...v18.1.0) (2021-02-22)


### Bug Fixes

* [#37](https://gitlab.com/commonground/core/design-system/issues/37) update TextInput placeholder color ([9d86191](https://gitlab.com/commonground/core/design-system/commit/9d861916667caddaef774d423a1719a423b9e49b))


### Features

* [#38](https://gitlab.com/commonground/core/design-system/issues/38) add textinput story with placeholder ([84698de](https://gitlab.com/commonground/core/design-system/commit/84698decf566bac35690dc9320feea5960a8d5d8))

## [18.0.2](https://gitlab.com/commonground/core/design-system/compare/v18.0.1...v18.0.2) (2021-02-22)


### Bug Fixes

* [#37](https://gitlab.com/commonground/core/design-system/issues/37) change placeholder text to gray600 ([a3789b3](https://gitlab.com/commonground/core/design-system/commit/a3789b37118795cba00cda10e418bdc1272e2b02))

## [18.0.1](https://gitlab.com/commonground/core/design-system/compare/v18.0.0...v18.0.1) (2021-02-16)


### Bug Fixes

* build ([29ff3ff](https://gitlab.com/commonground/core/design-system/commit/29ff3ffa37a225cd748338b3ad17416f7bebe76b))

# [18.0.0](https://gitlab.com/commonground/core/design-system/compare/v17.3.0...v18.0.0) (2021-02-16)


### Bug Fixes

* reset node modules ([89fd9b4](https://gitlab.com/commonground/core/design-system/commit/89fd9b46d1907e99208ba9afc91d46d69733409b))


### Features

* **alert:** remove dependency on react-router for ActionButtons ([2a936e7](https://gitlab.com/commonground/core/design-system/commit/2a936e71a3a6337dd9165d2e0dd6686f598845c5))
* **table:** removed from design-system ([4d0fac7](https://gitlab.com/commonground/core/design-system/commit/4d0fac76be0bfa96749aedad860270bfa0014135))
* move font to peerDependencies ([354e818](https://gitlab.com/commonground/core/design-system/commit/354e818a191a49780b0befa60e944b931a45ca06))


### BREAKING CHANGES

* **table:** Removed Table component from design system.
It is only used in NLX management and needs a lot of work to remove
hard dependency on react-router whilst keeping it accessible.
* **alert:** only one ActionButton in Alert now. Set `as` prop
yourself: `<Alert.ActionButon as={Link} to="/path">link</Alert.ActionButton>`
* @fontsource/source-sans-pro now has to be
installed and imported manually

# [17.3.0](https://gitlab.com/commonground/core/design-system/compare/v17.2.0...v17.3.0) (2021-02-15)


### Features

* allow standard types in TextInput ([4f9e256](https://gitlab.com/commonground/core/design-system/commit/4f9e25688f531895a905318b76d18e14b63b88ea))

# [17.2.0](https://gitlab.com/commonground/core/design-system/compare/v17.1.0...v17.2.0) (2021-01-14)


### Features

* **collapsible:** add onToggle handler ([acfed62](https://gitlab.com/commonground/core/design-system/commit/acfed62df3cc4b48d4f9315fc2780110e63c5931))

# [17.1.0](https://gitlab.com/commonground/core/design-system/compare/v17.0.4...v17.1.0) (2021-01-13)


### Features

* **collapsible:** enable disabling the animation ([d528363](https://gitlab.com/commonground/core/design-system/commit/d5283631c81d9179198c936ff19e4effad806544))
* **collapsible:** prevent toggle when clicking the collapsable content ([c1fda11](https://gitlab.com/commonground/core/design-system/commit/c1fda118b587d008e8b915098de9150e7d21624c))

## [17.0.4](https://gitlab.com/commonground/core/design-system/compare/v17.0.3...v17.0.4) (2021-01-11)


### Bug Fixes

* do not use document in global namespace to remain compatible with Gatsby ([029e230](https://gitlab.com/commonground/core/design-system/commit/029e230ffadf4ba36940fb1450599bf8fa906aa4))

## [17.0.3](https://gitlab.com/commonground/core/design-system/compare/v17.0.2...v17.0.3) (2020-12-23)


### Bug Fixes

* **button:** disabled links become buttons ([e3637a7](https://gitlab.com/commonground/core/design-system/commit/e3637a7e52a291971eb24beaff1768fc375513d2))

## [17.0.2](https://gitlab.com/commonground/core/design-system/compare/v17.0.1...v17.0.2) (2020-12-17)


### Bug Fixes

* **collapsible:** ensure the id for the collapsed content is being passed ([561716e](https://gitlab.com/commonground/core/design-system/commit/561716e008a25dc5be43dd118e8fabfc2465904e)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)

## [17.0.1](https://gitlab.com/commonground/core/design-system/compare/v17.0.0...v17.0.1) (2020-12-17)


### Bug Fixes

* **theme:** dark theme blue link color ([951b570](https://gitlab.com/commonground/core/design-system/commit/951b570b5a9e906c43dcc579121df293ce06b898))

# [17.0.0](https://gitlab.com/commonground/core/design-system/compare/v16.2.0...v17.0.0) (2020-12-16)


### Bug Fixes

* **select:** export Select component correctly ([0aacdac](https://gitlab.com/commonground/core/design-system/commit/0aacdacd22864485d725048a8ccce0c2db04a727)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)


### Features

* **select:** rename Select components ([7795d53](https://gitlab.com/commonground/core/design-system/commit/7795d537b8f7f466434c81fcb93f38ef297bad65)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)


### BREAKING CHANGES

* **select:** renamed SelectInput -> SelectComponent and FormikSelectInput -> SelectFormik

# [16.2.0](https://gitlab.com/commonground/core/design-system/compare/v16.1.1...v16.2.0) (2020-12-16)


### Features

* **select:** add component ([7451316](https://gitlab.com/commonground/core/design-system/commit/7451316862ae6f1618262edde411ad4b728ad217)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)
* **select:** add support for multiple sizes ([b0159e8](https://gitlab.com/commonground/core/design-system/commit/b0159e8d4dba0697fece5e0e41c021d602439640)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)
* **select:** apply design feedback ([5ef8631](https://gitlab.com/commonground/core/design-system/commit/5ef86313eaa8069d5ff3e8054aedad0bd36fcfb5)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)
* **select:** default Select component includes label and feedback ([ee6403c](https://gitlab.com/commonground/core/design-system/commit/ee6403c05b1c807356787282182372ee2a9b2986)), closes [commonground/nlx/nlx#1071](https://gitlab.com/commonground/nlx/nlx/issues/1071)

## [16.1.1](https://gitlab.com/commonground/core/design-system/compare/v16.1.0...v16.1.1) (2020-12-10)


### Bug Fixes

* **drawer:** remove debug artefact from rendering ([4897e7a](https://gitlab.com/commonground/core/design-system/commit/4897e7ae5716964c0e9da840d6214a235a389179))

# [16.1.0](https://gitlab.com/commonground/core/design-system/compare/v16.0.1...v16.1.0) (2020-12-10)


### Features

* **drawer:** add styling for focus state to the CloseButton ([5cdf145](https://gitlab.com/commonground/core/design-system/commit/5cdf1456df4fa4dc33ad6938cc521b0992b917d5))
* **drawer:** enable clicking outside Drawer when the mask is disabled ([3a2c56b](https://gitlab.com/commonground/core/design-system/commit/3a2c56b49639d6a9e62343d264ad5c6cec02e8f6))
* **drawer:** enable clicking outside StackedDrawer when the mask is disabled ([661fe72](https://gitlab.com/commonground/core/design-system/commit/661fe7274f4c9034e36cbf400aa538e59cb55ae3))

## [16.0.1](https://gitlab.com/commonground/core/design-system/compare/v16.0.0...v16.0.1) (2020-12-09)


### Bug Fixes

* **button:** style disabled state ([58db0ce](https://gitlab.com/commonground/core/design-system/commit/58db0ce432f984459c516f028175945e1ff16642))

# [16.0.0](https://gitlab.com/commonground/core/design-system/compare/v15.1.2...v16.0.0) (2020-12-09)


### Bug Fixes

* **button:** allow as prop on custom styled Button ([974af29](https://gitlab.com/commonground/core/design-system/commit/974af2931c8fb1cb82bd28074317402477e7adab))


### Build System

* fix type-o in releaserc ([8c7b167](https://gitlab.com/commonground/core/design-system/commit/8c7b1677cea50e33105418687be593c1a33c1011))


### BREAKING CHANGES

* the previous publish failed because of this
releaserc type-o. It was a breaking change, so we trigger one now.
* **button:** Buttons with prop `forwardedAs` will break.
This was a temporary workaround. Please use `as` now instead.

## [15.1.2](https://gitlab.com/commonground/core/design-system/compare/v15.1.1...v15.1.2) (2020-12-08)


### Performance Improvements

* get rid of polished ([1aa03e9](https://gitlab.com/commonground/core/design-system/commit/1aa03e9d461042a6679dd58b1c61748c1a046af3))

## [15.1.1](https://gitlab.com/commonground/core/design-system/compare/v15.1.0...v15.1.1) (2020-12-08)


### Bug Fixes

* **collapsible:** open animation ([3bfc558](https://gitlab.com/commonground/core/design-system/commit/3bfc558203d06e8d73d9b179a97b88f15c881e09))

# [15.1.0](https://gitlab.com/commonground/core/design-system/compare/v15.0.0...v15.1.0) (2020-12-01)


### Features

* **globalstyles:** responsive headings ([e125d57](https://gitlab.com/commonground/core/design-system/commit/e125d571518344910b9cc2c24a514145cf36e3a3))

# [15.0.0](https://gitlab.com/commonground/core/design-system/compare/v14.4.1...v15.0.0) (2020-11-30)


### Features

* **table:** rename $selected property to selected ([aeee71d](https://gitlab.com/commonground/core/design-system/commit/aeee71df7940d25a3956ce1ded9ca4326fe48e14))


### BREAKING CHANGES

* **table:** We don't want to expose the specific Transient Props feature
from Styled Components to our consumers.
Reference https://styled-components.com/docs/api#transient-props

## [14.4.1](https://gitlab.com/commonground/core/design-system/compare/v14.4.0...v14.4.1) (2020-11-25)


### Bug Fixes

* **drawer:** always capture focus ([1e75de5](https://gitlab.com/commonground/core/design-system/commit/1e75de5f840e7c8acf5bb09910136615112423dc))

# [14.4.0](https://gitlab.com/commonground/core/design-system/compare/v14.3.0...v14.4.0) (2020-11-24)


### Bug Fixes

* **toaster:** state update when unmounted ([5b20298](https://gitlab.com/commonground/core/design-system/commit/5b20298d99d76290d952416f0afbddcc91dedebc))


### Features

* **toaster:** introduced delay prop ([4af29ff](https://gitlab.com/commonground/core/design-system/commit/4af29ff1c5933c45ae8f8cd7f694360e3b85c095))


### Performance Improvements

* only create toasterRootDiv once ([f769de5](https://gitlab.com/commonground/core/design-system/commit/f769de5cc5313f45d13d185ebae03f2e15ce7e3d))

# [14.3.0](https://gitlab.com/commonground/core/design-system/compare/v14.2.3...v14.3.0) (2020-11-24)


### Features

* **table:** add selected state & styling for table rows ([550371c](https://gitlab.com/commonground/core/design-system/commit/550371c8650387f038102a25484a03f15cdfc49e))

## [14.2.3](https://gitlab.com/commonground/core/design-system/compare/v14.2.2...v14.2.3) (2020-11-23)


### Bug Fixes

* **toaster:** animation ([651e80b](https://gitlab.com/commonground/core/design-system/commit/651e80b73e5cd24676ea3304d2ac5cd253e7aa41))

## [14.2.2](https://gitlab.com/commonground/core/design-system/compare/v14.2.1...v14.2.2) (2020-11-18)


### Bug Fixes

* **drawer:** padding-right stacked drawer ([db6e694](https://gitlab.com/commonground/core/design-system/commit/db6e694aa035134f6169a19b7b5dc3926beae072))

## [14.2.1](https://gitlab.com/commonground/core/design-system/compare/v14.2.0...v14.2.1) (2020-11-17)


### Bug Fixes

* normalize alert and toaster icons to 20px ([c77aa12](https://gitlab.com/commonground/core/design-system/commit/c77aa1215eb49ec304da1561b603feb50a8df8d0))
* normalize chevron icon to 20px ([61ef788](https://gitlab.com/commonground/core/design-system/commit/61ef7881cf1883039fa120b2886133ad550c40aa))

# [14.2.0](https://gitlab.com/commonground/core/design-system/compare/v14.1.0...v14.2.0) (2020-11-16)


### Features

* **drawer:** add MockDrawerStack for easier testing ([3d3935f](https://gitlab.com/commonground/core/design-system/commit/3d3935febaf5d24b6e9307449d4473799d3af994))

# [14.1.0](https://gitlab.com/commonground/core/design-system/compare/v14.0.0...v14.1.0) (2020-11-16)


### Features

* **icon:** add support for multiple sizes ([d048499](https://gitlab.com/commonground/core/design-system/commit/d04849993c3b511af926ab7df97e7110c01f94e2))

# [14.0.0](https://gitlab.com/commonground/core/design-system/compare/v13.8.1...v14.0.0) (2020-11-12)


### Bug Fixes

* **drawer:** better StackedDrawer api ([4bdc1ce](https://gitlab.com/commonground/core/design-system/commit/4bdc1ce0e7182a70d39dd17ab0ebbdf5880e2535))


### BREAKING CHANGES

* **drawer:** - New way of creating StackedDrawer, see docs
- No longer exposing DrawerContext on Drawer compound component

## [13.8.1](https://gitlab.com/commonground/core/design-system/compare/v13.8.0...v13.8.1) (2020-11-11)


### Bug Fixes

* **icon:** prevent `icon` prop being passed to svg element ([0db2eaf](https://gitlab.com/commonground/core/design-system/commit/0db2eafd3a93eb7c277ab3b5bd651408fde24aac))

# [13.8.0](https://gitlab.com/commonground/core/design-system/compare/v13.7.0...v13.8.0) (2020-11-10)


### Features

* **icon:** replace InlineIcon with Icon ([fe4c5fa](https://gitlab.com/commonground/core/design-system/commit/fe4c5fa00fd663ff8e159e0c538b568913856a29))

# [13.7.0](https://gitlab.com/commonground/core/design-system/compare/v13.6.1...v13.7.0) (2020-11-10)


### Features

* **errormessage:** expose component ([5c35ae9](https://gitlab.com/commonground/core/design-system/commit/5c35ae9efd988519994e1e510c73bc22455e9a8a))

## [13.6.1](https://gitlab.com/commonground/core/design-system/compare/v13.6.0...v13.6.1) (2020-11-05)


### Bug Fixes

* allow all ranges of Formik 2.x as peerDependencies ([b7538a9](https://gitlab.com/commonground/core/design-system/commit/b7538a9a16b1597c21c323afe4edd6ff4a2a0662)), closes [#1081](https://gitlab.com/commonground/core/design-system/issues/1081)
* allow React v16.x and v17.x as peerDependencies ([b70790a](https://gitlab.com/commonground/core/design-system/commit/b70790ae2e4e5fd80a27bbbe1da119fb4e090e24)), closes [#1081](https://gitlab.com/commonground/core/design-system/issues/1081)

# [13.6.0](https://gitlab.com/commonground/core/design-system/compare/v13.5.1...v13.6.0) (2020-11-03)


### Bug Fixes

* better icon support in buttons ([ae5a14e](https://gitlab.com/commonground/core/design-system/commit/ae5a14e00ddd59b0ecb4a79b206280da6485b237))


### Features

* **inlinebutton:** introduce the InlineButton component ([2ac11b7](https://gitlab.com/commonground/core/design-system/commit/2ac11b7adc659a3d83882d21d86ebb9dec5ba51c))

## [13.5.1](https://gitlab.com/commonground/core/design-system/compare/v13.5.0...v13.5.1) (2020-10-29)


### Bug Fixes

* storybook ui after react17 upgrade ([cf32a12](https://gitlab.com/commonground/core/design-system/commit/cf32a12b3ec0da4cbef5c79e7b7742ac41e6828f))

# [13.5.0](https://gitlab.com/commonground/core/design-system/compare/v13.4.2...v13.5.0) (2020-10-19)


### Features

* **alert:** add sensible warning for wrong action button in propTypes ([98aa931](https://gitlab.com/commonground/core/design-system/commit/98aa931a9605c14bef90c15ea85cd1580cdcba3f))

## [13.4.2](https://gitlab.com/commonground/core/design-system/compare/v13.4.1...v13.4.2) (2020-10-19)


### Bug Fixes

* double hash in color ([e18f777](https://gitlab.com/commonground/core/design-system/commit/e18f7771cfcf8eeb431e0d7bd4201f381703b219))

## [13.4.1](https://gitlab.com/commonground/core/design-system/compare/v13.4.0...v13.4.1) (2020-10-16)


### Bug Fixes

* **theme:** fix error color with colorBrand3 bg ([aac27f1](https://gitlab.com/commonground/core/design-system/commit/aac27f10cb0a36277841b17daada259194ab2073))

# [13.4.0](https://gitlab.com/commonground/core/design-system/compare/v13.3.0...v13.4.0) (2020-10-13)


### Features

* **alert:** improve spacing for action buttons ([3891ce8](https://gitlab.com/commonground/core/design-system/commit/3891ce8de492c91fb12e606af7a20becd621f406))

# [13.3.0](https://gitlab.com/commonground/core/design-system/compare/v13.2.0...v13.3.0) (2020-10-12)


### Features

* **alert:** add support for actions ([6542b46](https://gitlab.com/commonground/core/design-system/commit/6542b46284588b3dbb912d00dd70c4b0f89e7f12))
* **button:** add size property ([232ec45](https://gitlab.com/commonground/core/design-system/commit/232ec45ac7c22762c6b4f60859ca2c733f192270))

# [13.2.0](https://gitlab.com/commonground/core/design-system/compare/v13.1.0...v13.2.0) (2020-10-08)


### Features

* **drawer:** stacking drawers ([6b0b893](https://gitlab.com/commonground/core/design-system/commit/6b0b893c1a9a124d1385268be008ffaf6871a88e))

# [13.1.0](https://gitlab.com/commonground/core/design-system/compare/v13.0.1...v13.1.0) (2020-10-06)


### Features

* **button:** add link variant ([515d4a0](https://gitlab.com/commonground/core/design-system/commit/515d4a013cc570491db99901704a48c7781de04a))

## [13.0.1](https://gitlab.com/commonground/core/design-system/compare/v13.0.0...v13.0.1) (2020-09-07)


### Bug Fixes

* relax peerDependencies to allow patches ([6ce7df9](https://gitlab.com/commonground/core/design-system/commit/6ce7df96b6b1b60ad9ba672f1c749d8732331e05))

# [13.0.0](https://gitlab.com/commonground/core/design-system/compare/v12.6.0...v13.0.0) (2020-09-03)


### Features

* **toaster:** reposition Toasts bottom/right ([f520098](https://gitlab.com/commonground/core/design-system/commit/f5200988d1cf91ab815816becbe0cca7d8e1b61e))
* **toaster:** teasers are now removed after 5s timeout ([ec8a3bf](https://gitlab.com/commonground/core/design-system/commit/ec8a3bfe67084de05acae12cc243cd6f817350c0))


### BREAKING CHANGES

* **toaster:** `position` prop is removed.

# [12.6.0](https://gitlab.com/commonground/core/design-system/compare/v12.5.0...v12.6.0) (2020-09-03)


### Features

* **drawer:** add option to skip opening animation ([fad814e](https://gitlab.com/commonground/core/design-system/commit/fad814eb1c96ba3905355e87f0b5862931ca8b16))

# [12.5.0](https://gitlab.com/commonground/core/design-system/compare/v12.4.3...v12.5.0) (2020-06-19)


### Features

* **collapsible:** add aria attributes ([de22af7](https://gitlab.com/commonground/core/design-system/commit/de22af701a34e74b21496a47ef6b201babb50903))

## [12.4.3](https://gitlab.com/commonground/core/design-system/compare/v12.4.2...v12.4.3) (2020-06-11)


### Bug Fixes

* **button:** fix contrast on dark theme ([de9e8f7](https://gitlab.com/commonground/core/design-system/commit/de9e8f7b15b9498084165f1d5bfa5a8e7cb34d44))

## [12.4.2](https://gitlab.com/commonground/core/design-system/compare/v12.4.1...v12.4.2) (2020-06-09)


### Bug Fixes

* drawer content scrolling and bg positioning ([165a071](https://gitlab.com/commonground/core/design-system/commit/165a071cc715b3baf35601ea966888d7f14ad209))

## [12.4.1](https://gitlab.com/commonground/core/design-system/compare/v12.4.0...v12.4.1) (2020-06-09)


### Bug Fixes

* **button:** secondary color ([db2bf84](https://gitlab.com/commonground/core/design-system/commit/db2bf84c4f073d3536e05986737d4a1e7e43c347))

# [12.4.0](https://gitlab.com/commonground/core/design-system/compare/v12.3.1...v12.4.0) (2020-06-05)


### Features

* **collapsible:** introduce component ([3daf60c](https://gitlab.com/commonground/core/design-system/commit/3daf60c24372d17ed35649134c51c4d4f04ac4fe)), closes [#8](https://gitlab.com/commonground/core/design-system/issues/8)
* **iconflippingchevron:** introduce component ([12867f2](https://gitlab.com/commonground/core/design-system/commit/12867f25568ac4ba9cacaa063b886037b5811552)), closes [#8](https://gitlab.com/commonground/core/design-system/issues/8)

## [12.3.1](https://gitlab.com/commonground/core/design-system/compare/v12.3.0...v12.3.1) (2020-06-02)


### Bug Fixes

* removes deprecation warning during build ([7cf79c2](https://gitlab.com/commonground/core/design-system/commit/7cf79c214140c143fa71f6b202fc24f80ffede7d))

# [12.3.0](https://gitlab.com/commonground/core/design-system/compare/v12.2.0...v12.3.0) (2020-06-02)


### Bug Fixes

* temp test for review app ([5a95396](https://gitlab.com/commonground/core/design-system/commit/5a953961ca79ee694210faa0bccc4c2816b4eaf2))
* toast component was not exported ([196f129](https://gitlab.com/commonground/core/design-system/commit/196f12962402b3ee89d789f414f878f1d9b1bb52))


### Features

* added Toast component and some tweaks ([d677096](https://gitlab.com/commonground/core/design-system/commit/d6770962f42cb8f4b78c182694081638af4fb600))
* toaster component ([e09c5c0](https://gitlab.com/commonground/core/design-system/commit/e09c5c029a9fce756026970e92943d3880864ebc))

# [12.2.0](https://gitlab.com/commonground/core/design-system/compare/v12.1.2...v12.2.0) (2020-05-29)


### Features

* **table:** add horizontal padding to cells ([dd9be3d](https://gitlab.com/commonground/core/design-system/commit/dd9be3da03891fdb9432360387bed7de4b0af853))
* **table:** introduce component ([e098765](https://gitlab.com/commonground/core/design-system/commit/e098765f0125a639498000fe0ae6fe053783fad8))

## [12.1.2](https://gitlab.com/commonground/core/design-system/compare/v12.1.1...v12.1.2) (2020-04-28)


### Bug Fixes

* **drawer:** disable focusOn when using noMask ([6f3157a](https://gitlab.com/commonground/core/design-system/commit/6f3157a41c5780ee87eb029ec10dfe126b28e1ba))

## [12.1.1](https://gitlab.com/commonground/core/design-system/compare/v12.1.0...v12.1.1) (2020-04-24)


### Bug Fixes

* **drawer:** make sure props for Header are being passed on ([3a98afc](https://gitlab.com/commonground/core/design-system/commit/3a98afcb107cd3a0f6b098d02faffd2790a6f396))

# [12.1.0](https://gitlab.com/commonground/core/design-system/compare/v12.0.0...v12.1.0) (2020-04-24)


### Features

* **drawer:** add noMask property to disable mask ([8613376](https://gitlab.com/commonground/core/design-system/commit/86133765eb309f9481744c38b31ba6e4c5fbd3f9)), closes [commonground/nlx/nlx#923](https://gitlab.com/commonground/nlx/nlx/issues/923)

# [12.0.0](https://gitlab.com/commonground/core/design-system/compare/v11.2.1...v12.0.0) (2020-04-24)


### Features

* **drawer:** improve default ui ([7959abd](https://gitlab.com/commonground/core/design-system/commit/7959abd7544b3dfed35f976497cb4613bd7c8447)), closes [commonground/nlx/nlx#923](https://gitlab.com/commonground/nlx/nlx/issues/923)


### BREAKING CHANGES

* **drawer:** When using the Drawer, you should provide both the header and content section manually. We've provided the `Drawer.Header` and `Drawer.Content` for this.

If you do have the need for customizing the header or content, you can provide your own components as children to the Drawer.

## [11.2.1](https://gitlab.com/commonground/core/design-system/compare/v11.2.0...v11.2.1) (2020-04-24)


### Bug Fixes

* add styling for disabled form fields ([55f57a3](https://gitlab.com/commonground/core/design-system/commit/55f57a338c51c9ddbbc4c0b058ca36533916d6e5))
* checkbox color was not applied correctly ([0d807c5](https://gitlab.com/commonground/core/design-system/commit/0d807c59115300244cff6cbdb36f938a81c76811))
* improve checkbox focus style ([7bc15a1](https://gitlab.com/commonground/core/design-system/commit/7bc15a13ed1c40d0c41d5ca9f8603cb562ebfe1b))
* improve ui for radio button groups ([011e0d0](https://gitlab.com/commonground/core/design-system/commit/011e0d05ffe5a389837e79b8328c94d201cf05f1))
* update text color for labels ([0ec8513](https://gitlab.com/commonground/core/design-system/commit/0ec8513e21310481661916e3c4bd24664cec33e7))
* **button:** add style for the focus state ([5147114](https://gitlab.com/commonground/core/design-system/commit/5147114ea973e1c7c1c44097b2a9ef83772fc4d6))

# [11.2.0](https://gitlab.com/commonground/core/design-system/compare/v11.1.0...v11.2.0) (2020-04-20)


### Features

* **spinner:** add component ([ce7b96c](https://gitlab.com/commonground/core/design-system/commit/ce7b96c2f644b72cb9fe6b0a73d22eb036eaedde))

# [11.1.0](https://gitlab.com/commonground/core/design-system/compare/v11.0.1...v11.1.0) (2020-04-17)


### Features

* **button:** add danger variant ([fc06954](https://gitlab.com/commonground/core/design-system/commit/fc069541df6a572ee85751f94c587a8fb8f4bd83))

## [11.0.1](https://gitlab.com/commonground/core/design-system/compare/v11.0.0...v11.0.1) (2020-04-17)


### Bug Fixes

* display radio label above the options ([01e987d](https://gitlab.com/commonground/core/design-system/commit/01e987d97e89be75abf58d4ea51feb240fb2a174))

# [11.0.0](https://gitlab.com/commonground/core/design-system/compare/v10.1.3...v11.0.0) (2020-04-17)


### Features

* **alert:** add alert role ([f9c129f](https://gitlab.com/commonground/core/design-system/commit/f9c129f0ead20b61a69f69acc1ca156564d743cd))
* **checkbox:** add component ([0403585](https://gitlab.com/commonground/core/design-system/commit/04035852cfe757bc98124e7ec31155e30ce4cde7))
* **input:** add pointer cursor for radio & checkboxes ([9c8e765](https://gitlab.com/commonground/core/design-system/commit/9c8e7652e402a8473f56b66e75304bede2fa1932))
* **input:** add styling for focus state ([54cdfa6](https://gitlab.com/commonground/core/design-system/commit/54cdfa69fbc512f79354e6e5ec922e284a6c6389))
* **input:** add styling for text inputs ([9af8a85](https://gitlab.com/commonground/core/design-system/commit/9af8a856057bd9b88e8e830278295c0d274c05ec))
* **input:** add support for different sizes ([a066ebb](https://gitlab.com/commonground/core/design-system/commit/a066ebb9c28d1b34511a2f610df09875e75376ff))
* **radio:** add component ([68df632](https://gitlab.com/commonground/core/design-system/commit/68df632d81cf3d1747bc2f7c6ab1c9cdace5b143))
* **textinput:** add component ([7147d45](https://gitlab.com/commonground/core/design-system/commit/7147d458de1ae6e414147510a9c3125e2f5f87d5))


### Reverts

* remove Input & LabelWithInput components ([1207faa](https://gitlab.com/commonground/core/design-system/commit/1207faaabb02525e7ad76a294c9dd76b48c3fd80))


### BREAKING CHANGES

* these components are being removed in favour of new input components (with a new API) which are coming up

## [10.1.3](https://gitlab.com/commonground/core/design-system/compare/v10.1.2...v10.1.3) (2020-04-09)


### Bug Fixes

* styled-components should not be bundled as a dependency ([c6ae261](https://gitlab.com/commonground/core/design-system/commit/c6ae261a31b612f43da39b869d8a239dddac4be5))

## [10.1.2](https://gitlab.com/commonground/core/design-system/compare/v10.1.1...v10.1.2) (2020-04-09)


### Bug Fixes

* generate the pages only on a release ([11759f7](https://gitlab.com/commonground/core/design-system/commit/11759f712d02cb5656cc779daab8353a514e6f83))

## [10.1.1](https://gitlab.com/commonground/core/design-system/compare/v10.1.0...v10.1.1) (2020-04-09)


### Bug Fixes

* **drawer:** only clicking outside the drawer should close it ([c7d9135](https://gitlab.com/commonground/core/design-system/commit/c7d91357b14eb8fc0634213bf1423f84294d087c))

# [10.1.0](https://gitlab.com/commonground/core/design-system/compare/v10.0.5...v10.1.0) (2020-04-08)


### Features

* **input:** introduce component with type checkbox and radio ([1b8401e](https://gitlab.com/commonground/core/design-system/commit/1b8401ec94c7b83f9e1e6535d269c8420c435926))

## [10.0.5](https://gitlab.com/commonground/core/design-system/compare/v10.0.4...v10.0.5) (2020-04-08)


### Bug Fixes

* dependencies ([4cf41fa](https://gitlab.com/commonground/core/design-system/commit/4cf41fa5c9254885b957d9af581f25cf252e1fb9))

## [10.0.4](https://gitlab.com/commonground/core/design-system/compare/v10.0.3...v10.0.4) (2020-04-06)


### Bug Fixes

* **button:** define missing proptypes ([1104a55](https://gitlab.com/commonground/core/design-system/commit/1104a550e24d1dddd1df38c6e7cc609fee615084))
* **nlxlogo:** specify default props ([95c8d1b](https://gitlab.com/commonground/core/design-system/commit/95c8d1b18c3c21d536ac42936e4804bbb26a33cd))

## [10.0.3](https://gitlab.com/commonground/core/design-system/compare/v10.0.2...v10.0.3) (2020-03-31)


### Bug Fixes

* **globalstyles:** update text color of small to use colorTextLabel ([d42ed22](https://gitlab.com/commonground/core/design-system/commit/d42ed222bd0ad1e58d0ee2d1ad2301a47d7d63e4))

## [10.0.2](https://gitlab.com/commonground/core/design-system/compare/v10.0.1...v10.0.2) (2020-03-30)


### Bug Fixes

* **GlobalStyles:** update text color of small to use colorTextLabel ([659820d](https://gitlab.com/commonground/core/design-system/commit/659820d0f7686c1f0f8d508feb4bff06f2a1f3c5))

## [10.0.1](https://gitlab.com/commonground/core/design-system/compare/v10.0.0...v10.0.1) (2020-03-27)


### Bug Fixes

* **Alert:** export component ([e7ae0f3](https://gitlab.com/commonground/core/design-system/commit/e7ae0f35a416013d1d6c2d0d686b1ad25e32f198))

# [10.0.0](https://gitlab.com/commonground/core/design-system/compare/v9.1.2...v10.0.0) (2020-03-27)


### Features

* **Alert:** add component ([a7f41cc](https://gitlab.com/commonground/core/design-system/commit/a7f41cc9fa5c03ca208e3439637bb4f5e14fbfc8))


### BREAKING CHANGES

* **Alert:** the previously known 'alert colors' have been removed and are now known as sub-colors

**Before:**

```
// Alerts
colorAlertInfo
colorAlertInfoLight
colorAlertWarning
colorAlertWarningLight
colorAlertError
colorAlertErrorLight
colorAlertSuccess
colorAlertSuccessLight
```

**After:**

```
// Sub-colors
colorInfo
colorInfoLight
colorWarning
colorWarningLight
colorError
colorErrorLight
colorSuccess
colorSuccessLight
```

## [9.1.2](https://gitlab.com/commonground/core/design-system/compare/v9.1.1...v9.1.2) (2020-03-26)


### Bug Fixes

* secondary button hover color ([72a432e](https://gitlab.com/commonground/core/design-system/commit/72a432e5c344e4b92cb7f5faa44ca96ee772a97c))

## [9.1.1](https://gitlab.com/commonground/core/design-system/compare/v9.1.0...v9.1.1) (2020-03-26)


### Bug Fixes

* **Drawer:** design updates ([6b32ba5](https://gitlab.com/commonground/core/design-system/commit/6b32ba5e7a025c57e609f1baf6ab50b92da613df))

# [9.1.0](https://gitlab.com/commonground/core/design-system/compare/v9.0.2...v9.1.0) (2020-03-26)


### Features

* **Drawer:** add component ([5a380dc](https://gitlab.com/commonground/core/design-system/commit/5a380dc782cf251c522ebe5fed48e5adbef02389)), closes [commonground/nlx/nlx#897](https://gitlab.com/commonground/nlx/nlx/issues/897)

## [9.0.2](https://gitlab.com/commonground/core/design-system/compare/v9.0.1...v9.0.2) (2020-03-24)


### Bug Fixes

* **button:** hover ([671dff8](https://gitlab.com/commonground/core/design-system/commit/671dff871b6d4a73a884c75a299ba9792b145989))

## [9.0.1](https://gitlab.com/commonground/core/design-system/compare/v9.0.0...v9.0.1) (2020-03-24)


### Bug Fixes

* **deps:** update dependency polished to v3.5.1 ([389afc4](https://gitlab.com/commonground/core/design-system/commit/389afc4eb520b18f8981f7118a84a1adf763fe9d))

# [9.0.0](https://gitlab.com/commonground/core/design-system/compare/v8.4.0...v9.0.0) (2020-03-20)


### deps

* upgrade dependency polished ([c27b0e7](https://gitlab.com/commonground/core/design-system/commit/c27b0e715ab73f13e06fafba24293d04827f6efa))


### BREAKING CHANGES

* triggering a breaking change here, since we forgot to do so in the previous MR. The structure & naming of the design tokens have been updated.

Previous MR: https://gitlab.com/commonground/core/design-system/-/merge_requests/156

# [8.4.0](https://gitlab.com/commonground/core/design-system/compare/v8.3.1...v8.4.0) (2020-03-19)


### Bug Fixes

* **Button:** update cursor to show pointer ([6a7f986](https://gitlab.com/commonground/core/design-system/commit/6a7f986122797132f78db8e4f894c79d0ba4ca67))


### Features

* **Button:** implement styling for disabled state ([bac08fc](https://gitlab.com/commonground/core/design-system/commit/bac08fc31ed309c877705db889651684ef61b90f))

## [8.3.1](https://gitlab.com/commonground/core/design-system/compare/v8.3.0...v8.3.1) (2020-03-16)


### Bug Fixes

* move polished from devDependencies to dependencies ([a801ea7](https://gitlab.com/commonground/core/design-system/commit/a801ea7734454a21cc6ba75cc7a50333f3482428))

# [8.3.0](https://gitlab.com/commonground/core/design-system/compare/v8.2.2...v8.3.0) (2020-03-12)


### Features

* **Button:** added component ([d381de2](https://gitlab.com/commonground/core/design-system/commit/d381de2bb379fd33d5abeb1ee7c691c22ef83081))

## [8.2.2](https://gitlab.com/commonground/core/design-system/compare/v8.2.1...v8.2.2) (2020-03-12)


### Bug Fixes

* **GlobalStyles:** do not use styled-components/macro ([272d55f](https://gitlab.com/commonground/core/design-system/commit/272d55f09c9d435caac0246c56d3329fd7c07f8c))

## [8.2.1](https://gitlab.com/commonground/core/design-system/compare/v8.2.0...v8.2.1) (2020-03-12)


### Bug Fixes

* **GlobalStyles:** export GlobalStyles component ([0fba713](https://gitlab.com/commonground/core/design-system/commit/0fba7132a60179df58e40bca7abdcc2f60107b55))

# [8.2.0](https://gitlab.com/commonground/core/design-system/compare/v8.1.0...v8.2.0) (2020-03-11)


### Features

* rework ordering of tokens ([9e6352b](https://gitlab.com/commonground/core/design-system/commit/9e6352be1782e73de05fe21733be36c12ae6e2b6))
* **GlobalStyles:** implement GlobalStyles component with a default theme ([9dfe5f4](https://gitlab.com/commonground/core/design-system/commit/9dfe5f423e34c66f006d6f53481f74b816cece5f))
* **VersionLogger:** remove component ([317abf1](https://gitlab.com/commonground/core/design-system/commit/317abf1f8c142eec4b63840e6fff30dcafb51a11))

# [8.1.0](https://gitlab.com/commonground/core/design-system/compare/v8.0.0...v8.1.0) (2020-03-10)


### Features

* **NLXLogo:** add white variant ([919251b](https://gitlab.com/commonground/core/design-system/commit/919251bc81cc388f0472fc3b320d8d83e9c3b2de))

# [8.0.0](https://gitlab.com/commonground/core/design-system/compare/v7.2.1...v8.0.0) (2020-03-05)


### chore

* **Card:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([2d66264](https://gitlab.com/commonground/core/design-system/commit/2d662648f2d500de829477cba1354047e8c00fb3))
* **Pagination:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([ad19374](https://gitlab.com/commonground/core/design-system/commit/ad19374953bdc0183fa8fbbe49caf0002e025c44))
* **Spinner:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([5fd53bd](https://gitlab.com/commonground/core/design-system/commit/5fd53bd7432d50a34fb6d8ae6ebac6dcf46504bc))
* **Table:** removed component nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([774583d](https://gitlab.com/commonground/core/design-system/commit/774583d9eef9023bd81b861a058e625e7bfde8f5))
* removed Container, Header, IconButton, Navigation & NLXNavbar components nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([7fad8cf](https://gitlab.com/commonground/core/design-system/commit/7fad8cfd7ce33ed971f7e48b2125979a8c740483))
* removed Search & SearchIcon components nlx[#872](https://gitlab.com/commonground/core/design-system/issues/872) ([a5a43dc](https://gitlab.com/commonground/core/design-system/commit/a5a43dc2f3a5bd58fb4090d3554856956064bac8))


### Features

* added tests for logos ([7777e2f](https://gitlab.com/commonground/core/design-system/commit/7777e2f198809c9313c5f7086b0004340d5c67bd))
* created RTL tests for VersionLogger ([b789ea0](https://gitlab.com/commonground/core/design-system/commit/b789ea01af7df7b2373a516961270d326b9c2f51))
* deprecated Pagination ([9e349cb](https://gitlab.com/commonground/core/design-system/commit/9e349cb061ff48772925ef48350ef68993d73d58))
* deprecation notice for Pagination ([fcd6602](https://gitlab.com/commonground/core/design-system/commit/fcd6602bf025c923944e6bc88278e007e3c44aa3))
* Pagination: tests rewritten in testing-library ([13d93ea](https://gitlab.com/commonground/core/design-system/commit/13d93eab3054fe3f4a19c4b8e10bbbc373bb8532))


### BREAKING CHANGES

* **Pagination:** the component has been extracted to its only consumer
* **Table:** the components have been extracted into its consumers
* **Card:** the components have been extracted into its consumers
* the components have been extracted into its only consumer - the Insight UI
* the components have been extracted into its only consumer - the Insight UI
* **Spinner:** the Spinner component has been extracted into its only consumer - the Directory UI

## [7.2.1](https://gitlab.com/commonground/cg-design-system/compare/v7.2.0...v7.2.1) (2019-10-25)


### Bug Fixes

* **gitlab-ci:** replace alpine with full docker base image ([a316291](https://gitlab.com/commonground/cg-design-system/commit/a31629115a026fdfa6aca9c5bec8522040617e97))

# [7.2.0](https://gitlab.com/commonground/cg-design-system/compare/v7.1.0...v7.2.0) (2019-09-05)


### Features

* **CGLogo:** add Common Ground logo ([1f8f339](https://gitlab.com/commonground/cg-design-system/commit/1f8f339))

# [7.1.0](https://gitlab.com/commonground/cg-design-system/compare/v7.0.0...v7.1.0) (2019-06-13)


### Features

* **VersionLogger:** add new component to log the tag from `/version.json` in the application this component is used in. ([1dd7a4e](https://gitlab.com/commonground/cg-design-system/commit/1dd7a4e))

# [7.0.0](https://gitlab.com/commonground/cg-design-system/compare/v6.0.0...v7.0.0) (2019-06-11)


### Features

* **Pagination:** rework Pagination props to perform the calculation for the amount of pages internally ([2de2a0c](https://gitlab.com/commonground/cg-design-system/commit/2de2a0c))


### BREAKING CHANGES

* **Pagination:** the amountOfPages property has been replaced by the
totalRows and rowsPerPage properties. Both are required.

# [6.0.0](https://gitlab.com/commonground/cg-design-system/compare/v5.1.0...v6.0.0) (2019-06-07)


### Bug Fixes

* **Pagination:** make previous and next button accessible ([04df6a9](https://gitlab.com/commonground/cg-design-system/commit/04df6a9))
* **Pagination:** require currentPage to be an integer ([44b0b11](https://gitlab.com/commonground/cg-design-system/commit/44b0b11))


### Features

* **Pagination:** increase spacing around input field ([8de8319](https://gitlab.com/commonground/cg-design-system/commit/8de8319))


### BREAKING CHANGES

* **Pagination:** before, it would be possible to provide a string as the
currentPage. This would lead to unexpected results. Eg. when passing '1'
as the currentPage, the onPageChangeHandler would pass '11' as the next
page. This fix makes sure the sum for the next page will output an
appropriate result.

# [5.1.0](https://gitlab.com/commonground/cg-design-system/compare/v5.0.0...v5.1.0) (2019-05-23)


### Bug Fixes

* **Pagination:** clicking the next button should trigger the ([78960aa](https://gitlab.com/commonground/cg-design-system/commit/78960aa))


### Features

* **Pagination:** added min and max attributes to the page input ([83db50b](https://gitlab.com/commonground/cg-design-system/commit/83db50b))

# [5.0.0](https://gitlab.com/commonground/cg-design-system/compare/v4.1.2...v5.0.0) (2019-05-08)


### Features

* **Search:** enable passing any props for the input field ([56d30b9](https://gitlab.com/commonground/cg-design-system/commit/56d30b9))


### BREAKING CHANGES

* **Search:** Search now takes an object for the input element
properties. The `inputId` and `inputName` property should be moved to
the `inputProps` object as `id` and `name` properties.

## [4.1.2](https://gitlab.com/commonground/cg-design-system/compare/v4.1.1...v4.1.2) (2019-05-03)


### Bug Fixes

* **ui:** increase contrast for active nav item ([ef9fa95](https://gitlab.com/commonground/cg-design-system/commit/ef9fa95))

## [4.1.1](https://gitlab.com/commonground/cg-design-system/compare/v4.1.0...v4.1.1) (2019-05-03)


### Bug Fixes

* release ([81d8393](https://gitlab.com/commonground/cg-design-system/commit/81d8393))

# [4.1.0](https://gitlab.com/commonground/cg-design-system/compare/v4.0.0...v4.1.0) (2019-05-02)


### Features

* **IconButton:** export component + add stories ([b718b0b](https://gitlab.com/commonground/cg-design-system/commit/b718b0b))
* **Table:** set background color to white for the BodyCell component ([c1fb4ce](https://gitlab.com/commonground/cg-design-system/commit/c1fb4ce))

# [4.0.0](https://gitlab.com/commonground/cg-design-system/compare/v3.1.0...v4.0.0) (2019-04-25)


### Bug Fixes

* **Navbar:** remove margin-right: auto; styling property ([3561488](https://gitlab.com/commonground/cg-design-system/commit/3561488))


### Features

* **Search:** enable setting the name and id attribute for the input field ([62c5a5d](https://gitlab.com/commonground/cg-design-system/commit/62c5a5d))


### BREAKING CHANGES

* **Navbar:** this property was causing issues for DON. The Navigation
component inside a flexbox element with justify-content was not taking
up the expected space, because of it's margin-right: auto; setting.

# [3.1.0](https://gitlab.com/commonground/cg-design-system/compare/v3.0.0...v3.1.0) (2019-04-25)


### Features

* **Pagination:** implement Pagination component ([cb9de5e](https://gitlab.com/commonground/cg-design-system/commit/cb9de5e))

# [3.0.0](https://gitlab.com/commonground/cg-design-system/compare/v2.0.0...v3.0.0) (2019-04-19)


### chore

* trigger new release ([80abe69](https://gitlab.com/commonground/cg-design-system/commit/80abe69))


### BREAKING CHANGES

* previous release (v2.0.0) was already tagged in GitLab.
This caused the deployment to fail.

# [2.0.0](https://gitlab.com/commonground/cg-design-system/compare/v1.0.3...v2.0.0) (2019-04-19)


### chore

* **Spinner:** re-name component files to index.js ([f7c6040](https://gitlab.com/commonground/cg-design-system/commit/f7c6040))
* rework Navbar into separate components + add NLXNavbar ([d60445e](https://gitlab.com/commonground/cg-design-system/commit/d60445e))


### Features

* **GitLabLogo:** add GitLabLogo as component ([a388ab5](https://gitlab.com/commonground/cg-design-system/commit/a388ab5))
* **Navbar:** add Navbar as component ([4eb3ace](https://gitlab.com/commonground/cg-design-system/commit/4eb3ace))
* **Navbar:** enable customizing the navigation URLs ([9e0905c](https://gitlab.com/commonground/cg-design-system/commit/9e0905c))
* **Navbar:** pass down props to its container ([abdd736](https://gitlab.com/commonground/cg-design-system/commit/abdd736))
* **NLXLogo:** add NLXLogo as component ([48dc435](https://gitlab.com/commonground/cg-design-system/commit/48dc435))
* **Search:** add Search component ([a05dfc0](https://gitlab.com/commonground/cg-design-system/commit/a05dfc0))
* **Search:** enable custom placeholder ([3e90b8c](https://gitlab.com/commonground/cg-design-system/commit/3e90b8c))
* **Table:** add Table as component ([0825a7c](https://gitlab.com/commonground/cg-design-system/commit/0825a7c))


### BREAKING CHANGES

* Navbar has been renamed to NLXNavbar and requires
Navbar.Item components as children.
* **Navbar:** URLs must be passed to the Navbar component
* **Spinner:** filenames have been updated. Import of the component
files should be updated.

## [1.0.3](https://gitlab.com/commonground/cg-design-system/compare/v1.0.2...v1.0.3) (2019-01-23)


### Bug Fixes

* **README:** prefix relative markdown links with ./ ([ede4110](https://gitlab.com/commonground/cg-design-system/commit/ede4110)), closes [#1](https://gitlab.com/commonground/cg-design-system/issues/1)

## [1.0.2](https://gitlab.com/commonground/cg-design-system/compare/v1.0.1...v1.0.2) (2019-01-23)


### Bug Fixes

* **README:** update Git URL to work with README links + rewrite links ([75225ab](https://gitlab.com/commonground/cg-design-system/commit/75225ab)), closes [#1](https://gitlab.com/commonground/cg-design-system/issues/1)

## [1.0.1](https://gitlab.com/commonground/cg-design-system/compare/v1.0.0...v1.0.1) (2019-01-23)


### Bug Fixes

* **README:** include docs in NPM package ([e30557e](https://gitlab.com/commonground/cg-design-system/commit/e30557e)), closes [#1](https://gitlab.com/commonground/cg-design-system/issues/1)

# 1.0.0 (2019-01-17)


### Bug Fixes

* **docs:** reword release process ([810dd89](https://gitlab.com/commonground/cg-design-system/commit/810dd89))
* use cross-env for Windows compatibility ([932d3a6](https://gitlab.com/commonground/cg-design-system/commit/932d3a6))


### chore

* fix publishing npm package by enabling unsafe-perm for rm -rf ([28b7640](https://gitlab.com/commonground/cg-design-system/commit/28b7640))
* install missing dependency [@semantic-release](https://gitlab.com/semantic-release)/changelog ([f3d6fad](https://gitlab.com/commonground/cg-design-system/commit/f3d6fad))
* release transpiled assets ([e5475c8](https://gitlab.com/commonground/cg-design-system/commit/e5475c8))
* release transpiled assets ([0960cdc](https://gitlab.com/commonground/cg-design-system/commit/0960cdc))
* replace [@semantic-release](https://gitlab.com/semantic-release)/gitlab-config with [@semantic-release](https://gitlab.com/semantic-release)/gitlab ([95a7626](https://gitlab.com/commonground/cg-design-system/commit/95a7626))
* replace prepublish with prebublishOnly ([d6af0e0](https://gitlab.com/commonground/cg-design-system/commit/d6af0e0)), closes [/github.com/npm/npm/issues/10074#issue-112707857](https://gitlab.com//github.com/npm/npm/issues/10074/issues/issue-112707857)
* replace rm -rf with rimraf ([28f2659](https://gitlab.com/commonground/cg-design-system/commit/28f2659))
* revert previous release ([3adf243](https://gitlab.com/commonground/cg-design-system/commit/3adf243))


### Documentation

* **readme:** remove whitespace ([e4b93da](https://gitlab.com/commonground/cg-design-system/commit/e4b93da))


### Features

* **Spinner:** expose Spinner component by setting NPM's main file ([8862c1a](https://gitlab.com/commonground/cg-design-system/commit/8862c1a))


### BREAKING CHANGES

* https://github.com/npm/npm/issues/2984#issuecomment-67626518
* prepublish is deprecated and also ran when installing
* prevent NPM error 'cannot run in wd'
* ignore test files and snapshots
* 
* trigger release to prepare for v1
* trigger release
* trigger release
* **readme:** Triggering a new release.
